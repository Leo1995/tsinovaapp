activate_this = '/var/www/TsinovaApp/TsinovaApp/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

import sys
import logging

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/TsinovaApp/")
sys.path.insert(1,"/var/www/TsinovaApp/tsinovaapp.wsgi")
reload(sys)
sys.setdefaultencoding('utf-8')

from TsinovaApp import app as application
application.secret_key = '78uijkm'
