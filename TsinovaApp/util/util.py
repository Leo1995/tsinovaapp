# -*- Coding: UTF-8 -*-
#coding: utf-8

from sqlalchemy.sql import func
from datetime import datetime, timedelta
from config import *
from decimal import Decimal

MESSAGE_SUCCESS_DATA_UPDATE_API = "Informações locais atualizadas com sucesso"
MESSAGE_ERROR_DATA_UPDATE_API = "Não foi possível buscar as informações do servidor para atualizar as informações locais."

# Verifica se os valores são iguais
def isEquals(value, value2):
    if value is None and value2 is None:
        return True
    if (value is None and value2 is not None) or (value is not None and value2 is None):
        return False
    if value == value2:
        return True
    return False

# Retorna o número formatado
def replaceNumAndConvertString(num, search_character, replacement_character):
    return str(num).replace(search_character, replacement_character)

# Formata os valores com as casas decimais
def formatValue(value, casas):
    new_value = int(value)
    compare_value = Decimal("{:.{}f}".format(value, casas))
    if compare_value == new_value:
        return new_value
    return compare_value

# Retorna a diferença em minutos entre duas datas
def getDifferenceMinutes(date1, date2):
    difference = date2 - date1
    return int(difference.total_seconds() / 60.0)

# Retorna a diferença em segundos entre duas datas
def getDifferenceSeconds(date1, date2):
    difference = date2 - date1
    return int(difference.total_seconds())

# Retorna a classe do range
def getClassRange(type, value):
    RANGE = None

    if type == 1:
        RANGE = RANGE_US
    if type == 2:
        RANGE = RANGE_PORTS_COPPER
    if type == 3:
        RANGE = RANGE_PORTS_FIBER
    if type == 4:
        RANGE = RANGE_POTENCY
    if type == 5:
        RANGE = RANGE_ELECTRICAL
    if type == 6:
        RANGE = RANGE_STANDARD_DEVIATION
    if type == 7:
        RANGE = RANGE_PORTS_SWITCH

    if RANGE is None:
        return ""

    for range_value in RANGE:
        if value >= range_value[0] and value <= range_value[1]:
            return range_value[2]

    return ""
