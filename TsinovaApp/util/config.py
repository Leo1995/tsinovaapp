# -*- Coding: UTF-8 -*-
#coding: utf-8

# URI de conexão com o banco de dados
SQLALCHEMY_DATABASE_URI = "mysql://tsinova:Ce55bb..@localhost/sysanalytical"

# UTF-8 FLASK
JSON_AS_ASCII = False

# Links API
API_DATA_UPDATE_ALL = "data-update-all.php"
API_DATA_UPDATE_LAST = "data-update-last.php"

# Tempo para atualização do banco de dados, em MINUTOS
DATA_UPDATE_INTERVAL = 15

# Parâmetros para calcular portas de cobre e portas de fibra
STATUS_PORTS_COPPER_AVAILABLE = [1]
STATUS_PORTS_COPPER_USED = [2]

STATUS_PORTS_FIBER_AVAILABLE = [1]
STATUS_PORTS_FIBER_USED = [2]

STATUS_PORTS_SWITCH_AVAILABLE = [1]
STATUS_PORTS_SWITCH_USED = [2]

# Range racks
RANGE_US = [[0, 50, "range_green"],\
            [51, 80, "range_yellow"], \
            [81, 100, "range_red"], \
            [101, 1000, "range_red"]]

RANGE_PORTS_SWITCH = [[0, 50, "range_green"],\
            [51, 80, "range_yellow"], \
            [81, 100, "range_red"], \
            [101, 1000, "range_red"]]

RANGE_PORTS_COPPER = [[0, 50, "range_green"],\
            [51, 80, "range_yellow"], \
            [81, 100, "range_red"], \
            [101, 1000, "range_red"]]

RANGE_PORTS_FIBER = [[0, 50, "range_green"],\
            [51, 80, "range_yellow"], \
            [81, 100, "range_red"], \
            [101, 1000, "range_red"]]

RANGE_POTENCY = [[0, 50, "range_green"],\
            [51, 80, "range_yellow"], \
            [81, 100, "range_red"], \
            [101, 1000, "range_red"]]

RANGE_ELECTRICAL = [[0, 50, "range_green"],\
            [51, 80, "range_yellow"], \
            [81, 100, "range_red"], \
            [101, 1000, "range_red"]]

RANGE_STANDARD_DEVIATION = [[0, 100, "range_green range_standard_deviation"],\
            [51, 80, "range_yellow range_standard_deviation"], \
            [81, 100, "range_red range_standard_deviation"]]
