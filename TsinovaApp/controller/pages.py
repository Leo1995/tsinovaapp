# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import app, db, login_manager
from flask import flash, render_template, request, redirect, url_for, jsonify, session, Markup
from flask_login import LoginManager, login_required, login_user, logout_user, current_user
import json
import md5
from copy import copy, deepcopy

import requests
from sqlalchemy.sql import func
from datetime import datetime, timedelta
from sqlalchemy import text

from util.config import *
from util.util import *

from model.vo.user import User
from model.vo.server import Server
from model.vo.server_user import ServerUser
from model.vo.building import Building
from model.vo.floor import Floor
from model.vo.room import Room
from model.vo.rack import Rack
from model.vo.group_rack import GroupRack
from model.vo.equipament import Equipament
from model.vo.equipament_ports import EquipamentPorts
from model.vo.company import Company
from model.vo.update_server_log import UpdateServerLog
from model.vo.server_simulation import ServerSimulation
from model.vo.ports_server_simulation import PortsServerSimulation
from model.vo.limiting import Limiting
from model.vo.group_connection import GroupConnection
from model.vo.racks_details import RacksDetails
from model.vo.building_details import BuildingDetails
from model.vo.simulation_future import SimulationFuture
from model.vo.work_order import WorkOrder
from model.vo.work_order_task import WorkOrderTask
from model.vo.electrical_parameters import ElectricalParameters

from model.dao.user_dao import UserDao
from model.dao.company_dao import CompanyDao
from model.dao.building_dao import BuildingDao
from model.dao.floor_dao import FloorDao
from model.dao.room_dao import RoomDao
from model.dao.group_rack_dao import GroupRackDao
from model.dao.rack_dao import RackDao
from model.dao.equipament_dao import EquipamentDao
from model.dao.equipament_ports_dao import EquipamentPortsDao
from model.dao.slot_dao import SlotDao
from model.dao.server_dao import ServerDao
from model.dao.group_connection_dao import GroupConnectionDao

# Página de login
@app.route("/", methods=["GET", "POST"])
@app.route("/login", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def pageLogin():
    if current_user.is_authenticated:
        return redirect("/admin/")
    if request.method == "GET":
        return render_template("login.html")
    if request.method == "POST":
        email = request.form['email'].strip()
        password = request.form['password'].strip()
        # exibe novamente a página caso e-mail ou senha inválidos
        if not email or not password:
            flash("Não foi possível realizar o login. Email e senha obrigatória!", "general")
            return render_template("login.html", email=email)
        # busca o usuário que possue as credenciais informadas
        user = User.query.filter_by(email = email, password = md5.md5(password).hexdigest()).first()
        # verifica se existe um usuário com as credenciais
        if not user or user is None:
            flash("E-mail ou senha inválidas!", "general")
            return render_template("login.html", email=email)
        # credenciais informada corretamente
        login_user(user)
        return redirect("/admin/update")


# --------------------------------------------------------------------------------------------------------------------------------- #


# Página de atualização dos dados
@app.route("/admin/update", methods=["GET", "POST"])
@login_required
def pageUpdate():
    if request.method == "GET":
        return render_template("admin/update.html")
    if request.method == "POST":
        data = []
        user = User.query.filter_by(id_user = current_user.id_user).first()
        for server in user.servers:
            if not server.off_air:
                continue
            if server.last_update_date is None:
                data.append(dataUpdateAll(server))
            else:
                data.append(dataUpdateLast(server))
        return jsonify(data)


# --------------------------------------------------------------------------------------------------------------------------------- #


def getWorkOrders(companies, company=None):
    work_orders = []
    if company is not None:
        api = company.server.api_imvision + "WorkOrders"
        data_json = []
        if not company.server.off_air:
            data_json = requests.get(api, auth=(company.server.user_api, company.server.password_api)).json()
        for data in data_json:
            work_order = WorkOrder()
            work_order.orderState = data['workOrderState']
            work_order.orderStateName = data['workOrderStateName']
            work_order.orderTypeName = data['workOrderTypeName']
            work_order.dateStart = datetime.strptime(data['startDate'], '%Y-%m-%dT%H:%M:%S')
            work_order.dateEnd = datetime.strptime(data['endDate'], '%Y-%m-%dT%H:%M:%S')
            work_order.description = data['description']
            work_order.name = data['name']
            if data['technician']:
                work_order.technician = data['technician']['firstName'] + " " + data['technician']['lastName']

            for task_json in data['tasks']:
                task = WorkOrderTask()
                task.taskStatus = task_json['workOrderTaskStatus']
                task.taskStatusName = task_json['workOrderTaskStatusName']
                task.taskType = task_json['workOrderTaskType']
                task.taskTypeName = task_json['workOrderTaskTypeName']
                task.name = task_json['name']
                task.description = task_json['description']
                work_order.tasks.append(task)

            work_orders.append(work_order)
    else:
        for company in companies:
            api = company.server.api_imvision + "WorkOrders"
            data_json = []
            if not company.server.off_air:
                data_json = requests.get(api, auth=(company.server.user_api, company.server.password_api)).json()
            for data in data_json:
                work_order = WorkOrder()
                work_order.orderState = data['workOrderState']
                work_order.orderStateName = data['workOrderStateName']
                work_order.orderTypeName = data['workOrderTypeName']
                work_order.dateStart = datetime.strptime(data['startDate'], '%Y-%m-%dT%H:%M:%S')
                work_order.dateEnd = datetime.strptime(data['endDate'], '%Y-%m-%dT%H:%M:%S')
                work_order.description = data['description']
                work_order.name = data['name']
                if data['technician']:
                    work_order.technician = data['technician']['firstName'] + " " + data['technician']['lastName']

                for task_json in data['tasks']:
                    task = WorkOrderTask()
                    task.taskStatus = task_json['workOrderTaskStatus']
                    task.taskStatusName = task_json['workOrderTaskStatusName']
                    task.taskType = task_json['workOrderTaskType']
                    task.taskTypeName = task_json['workOrderTaskTypeName']
                    task.name = task_json['name']
                    task.description = task_json['description']
                    work_order.tasks.append(task)

                work_orders.append(work_order)
    return work_orders

def getWorkOrdersByState(work_orders, state):
    list = []
    for work_order in work_orders:
        if work_order.orderState == state:
            list.append(work_order)
    return list

def getBuildingDetails(id_user, building=None):
    list = []

    if building is None:
        list = BuildingDetails.query.filter_by(id_user = id_user).all()
    else:
        list = BuildingDetails.query.filter_by(id_user = id_user, id_building = building.id_building).all()

    qtd_servers = 0
    qtd_switchs = 0
    qtd_pathpanels = 0
    qtd_blades = 0
    qtd_servers_blades = 0
    qtd_racks = 0
    qtd_ports_copper = 0
    qtd_ports_copper_used = 0
    qtd_ports_fiber = 0
    qtd_ports_fiber_used = 0
    qtd_ports_switch = 0
    qtd_ports_switch_used = 0

    for obj in list:
        qtd_servers += obj.qtd_servers
        qtd_switchs += obj.qtd_switchs
        qtd_pathpanels += obj.qtd_pathpanels
        qtd_blades += obj.qtd_blades
        qtd_servers_blades += obj.qtd_servers_blades
        qtd_racks += obj.qtd_racks
        qtd_ports_copper += obj.qtd_ports_copper_available + obj.qtd_ports_copper_used
        qtd_ports_copper_used += obj.qtd_ports_copper_used
        qtd_ports_fiber += obj.qtd_ports_fiber_available + obj.qtd_ports_fiber_used
        qtd_ports_fiber_used += obj.qtd_ports_fiber_used
        qtd_ports_switch += obj.qtd_ports_switch_available + obj.qtd_ports_switch_used
        qtd_ports_switch_used += obj.qtd_ports_switch_used

    def getValuePorcentageUsed(value, total):
        if total == 0:
            return 0.0
        return formatValue((value * 100) / total, 1)

    return {"qtd_servers": qtd_servers, "qtd_switchs": qtd_switchs, \
            "qtd_pathpanels": qtd_pathpanels, "qtd_blades": qtd_blades, \
            "qtd_servers_blades": qtd_servers_blades, \
            "qtd_racks": qtd_racks, \
            "qtd_ports_copper_used": getValuePorcentageUsed(qtd_ports_copper_used, qtd_ports_copper), \
            "qtd_ports_fiber_used": getValuePorcentageUsed(qtd_ports_fiber_used, qtd_ports_fiber), \
            "qtd_ports_switch_used": getValuePorcentageUsed(qtd_ports_switch_used, qtd_ports_switch)}



def getWorkOrdersFromEventsJson(work_orders):
    list = []
    for work_order in work_orders:
        data = {'title': work_order.name, 'status': work_order.orderState, 'start': int(work_order.dateStart.strftime('%s')) * 1000, 'end': int(work_order.dateEnd.strftime('%s')) * 1000 }
        list.append(data)
    return list

# Página de início
@app.route("/admin/")
@app.route("/admin/index")
@app.route("/admin/home")
@login_required
def pageAdmin():
    user = User.query.filter_by(id_user = current_user.id_user).first()
    buildings = BuildingDao.getBuildingsByUser(user.id_user)
    companies = CompanyDao.getCompaniesByUser(user.id_user)

    # Parâmetros de URL
    building_selected_param = request.args.get("building", default=None, type=int)
    building_selected = None
    if building_selected_param:
        building_selected = Building.query.filter_by(id_building = building_selected_param).first()

    company_selected_param = request.args.get("site", default=None, type=int)
    company_selected = None
    if company_selected_param:
        company_selected = Company.query.filter_by(id_company = company_selected_param).first()

    building_details = getBuildingDetails(user.id_user, building_selected)

    work_orders = []

    try:
        work_orders = getWorkOrders(companies, company_selected)
    except:
        flash("Não foi possível buscar os eventos cadastrados no servidor do imvision.", "message-error")

    work_orders_immediate = getWorkOrdersByState(work_orders, 1)
    work_orders_scheduled = getWorkOrdersByState(work_orders, 2)
    work_orders_pending = getWorkOrdersByState(work_orders, 3)
    work_orders_onhold = getWorkOrdersByState(work_orders, 4)
    work_orders_completed = getWorkOrdersByState(work_orders, 5)
    work_orders_pastdue = getWorkOrdersByState(work_orders, 6)

    return render_template("admin/inicio.html", \
                           user=user, \
                           replaceNumAndConvertString=replaceNumAndConvertString, \
                           enumerate=enumerate, \
                           buildings=buildings, \
                           companies=companies,\
                           building_selected=building_selected, \
                           company_selected=company_selected, \
                           building_details=building_details, \
                           work_orders_immediate=work_orders_immediate, \
                           work_orders_scheduled=work_orders_scheduled, \
                           work_orders_pending=work_orders_pending, \
                           work_orders_onhold=work_orders_onhold, \
                           work_orders_completed=work_orders_completed, \
                           work_orders_pastdue=work_orders_pastdue, \
                           work_orders_events_json=getWorkOrdersFromEventsJson(work_orders))

# --------------------------------------------------------------------------------------------------------------------------------- #


# Exibe todos os racks
@app.route("/admin/racks", methods=["GET"])
@login_required
def pageRacks():
    user = User.query.filter_by(id_user = current_user.id_user).first()
    buildings = BuildingDao.getBuildingsByUser(user.id_user)
    racks = RacksDetails.query.filter_by(id_user=user.id_user, participate_calculation=1).all()
    racks_not_participate_calculation = RackDao.getRacksNoParticipateCalculationByUser(user.id_user)
    return render_template("admin/racks.html", \
                            racks=racks, \
                            racks_not_participate_calculation=racks_not_participate_calculation, \
                            enumerate=enumerate, \
                            user=user,
                            getClassRange=getClassRange,\
                            buildings=buildings)


# --------------------------------------------------------------------------------------------------------------------------------- #



# Exibe todos os servidores
@app.route("/admin/servers", methods=["GET", "POST"])
@login_required
def pageServers():
    user = User.query.filter_by(id_user = current_user.id_user).first()

    # Exibe a página
    if request.method == "GET":
        servers = Server.query.all()
        return render_template("admin/servers.html", enumerate=enumerate, servers=servers, user=user)

    # Cadastrar servidor
    if request.method == "POST":

        action = request.form['action'].strip()

        if action == "delete":
            id = int(request.form['id'].strip())
            try:
                ServerDao.removeServer(id)
                db.session.commit()
            except:
                db.session.rollback()
                flash("Falha ao excluir o servidor.", "message-error")
            else:
                flash("Servidor excluído com sucesso.", "message-success")

        if action == "clear":
            id = int(request.form['id'].strip())
            try:
                db.engine.execute(text("DELETE FROM company WHERE id_server = " + str(id)))
                server = Server.query.filter_by(id_server = id).first()
                '''for company in server.companies:
                    db.session.delete(company)
                    db.session.flush()'''
                server.last_update_date = None
                db.session.commit()
            except:
                db.session.rollback()
                flash("Falha ao apagar os dados do servidor.", "message-error")
            else:
                flash("Dados do servidor apagados com sucesso.", "message-success")


        if action == "add":
            name = request.form['name'].strip()
            url = request.form['url'].strip()
            ip_database = request.form['ip_database'].strip()
            user_database = request.form['user_database'].strip()
            password_database = request.form['password_database'].strip()
            url_api = request.form['url_api'].strip()
            user_api = request.form['user_api'].strip()
            password_api = request.form['password_api'].strip()
            off_air = request.form.getlist('off_air')
            # Valida as informações
            if not name or not url or not ip_database or not user_database or not password_database or not url_api or not user_api or not password_api:
                flash("Falha ao cadastrar o servidor. Nome, Url, Host do banco de dados, Usuário do banco de dados e Senha do banco de dados são obrigatórios!", "message-error")
                return redirect("/admin/servers")
            # Cadastra o servidor
            try:
                server = Server()
                server.name = name
                server.url = url
                server.ip_database = ip_database
                server.user_database = user_database
                server.password_database = password_database
                server.api_imvision = url_api
                server.user_api = user_api
                server.password_api = password_api
                server.off_air = False
                if off_air and len(off_air) > 0:
                    server.off_air = True
                ServerDao.insertServer(server)
                db.session.commit()
            except:
                raise
                db.session.rollback()
                flash("Falha ao cadastrar o servidor.", "message-error")
            else:
                flash("Servidor cadastrado com sucesso.", "message-success")

        if action == "update":
            id = request.form['id'].strip()
            name = request.form['name'].strip()
            url = request.form['url'].strip()
            ip_database = request.form['ip_database'].strip()
            user_database = request.form['user_database'].strip()
            password_database = request.form['password_database'].strip()
            url_api = request.form['url_api'].strip()
            user_api = request.form['user_api'].strip()
            password_api = request.form['password_api'].strip()
            off_air = request.form.getlist('off_air')
            # Valida as informações
            if not name or not url or not ip_database or not user_database or not password_database or not url_api or not user_api or not password_api:
                flash("Falha ao cadastrar o servidor. Nome, Url, Host do banco de dados, Usuário do banco de dados e Senha do banco de dados são obrigatórios!", "message-error")
                return redirect("/admin/servers")
            # Atualizar o servidor
            try:
                server = Server.query.filter_by(id_server=id).first()
                server.name = name
                server.url = url
                server.ip_database = ip_database
                server.user_database = user_database
                server.password_database = password_database
                server.api_imvision = url_api
                server.user_api = user_api
                server.password_api = password_api
                if off_air and len(off_air) > 0:
                    server.off_air = True
                else:
                    server.off_air = False
                db.session.flush()
                db.session.commit()
            except:
                db.session.rollback()
                flash("Falha ao atualizar o cadastro.", "message-error")
            else:
                flash("Cadastro atualizado com sucesso.", "message-success")

        # Exibe a página
        return redirect("/admin/servers")



# --------------------------------------------------------------------------------------------------------------------------------- #



# Exibe todos os usuários
@app.route("/admin/users", methods=["GET", "POST"])
@login_required
def pageUsers():
    user = User.query.filter_by(id_user = current_user.id_user).first()

    # Exibe a página
    if request.method == "GET":
        servers = Server.query.all()
        users = User.query.all()
        return render_template("admin/users.html", enumerate=enumerate, users=users, user=user, servers=servers)

    # Cadastrar servidor
    if request.method == "POST":

        action = request.form['action'].strip()

        if action == "delete":
            id = int(request.form['id'].strip())
            try:
                UserDao.removeUser(id)
                db.session.commit()
            except:
                db.session.rollback()
                flash("Falha ao excluir o usuário.", "message-error")
            else:
                flash("Usuário excluído com sucesso.", "message-success")

        if action == "add":
            name = request.form['name'].strip()
            email = request.form['email'].strip()
            password = request.form['password'].strip()
            servers = request.form.getlist('servers')
            # Valida as informações
            if not name or not email or not password:
                flash("Falha ao cadastrar o usuário. Nome, Email e Senha são obrigatórios!", "message-error")
                return redirect("/admin/users")
            # Cadastra o servidor
            try:
                user = User(name, email, md5.md5(password).hexdigest())
                for id_server in servers:
                    user.servers.append(Server.query.filter_by(id_server=id_server).first())
                UserDao.insertUser(user)
                db.session.commit()
            except:
                db.session.rollback()
                flash("Falha ao cadastrar o usuário.", "message-error")
            else:
                flash("Usuário cadastrado com sucesso.", "message-success")

        if action == "update":
            id = request.form['id'].strip()
            name = request.form['update_name'].strip()
            email = request.form['update_email'].strip()
            password = request.form['update_password'].strip()
            servers = request.form.getlist('update_servers')
            # Valida as informações
            if not name or not email:
                flash("Falha ao atualizar o cadastro. Nome e Email são obrigatórios!", "message-error")
                return redirect("/admin/users")
            # Atualizar o usuário
            try:
                user = User.query.filter_by(id_user=id).first()
                user.name = name
                user.email = email
                for server_user in user.server_users:
                    db.session.delete(server_user)
                    db.session.flush()
                for id_server in servers:
                    user.servers.append(Server.query.filter_by(id_server=id_server).first())
                    db.session.flush()
                if password:
                    user.password = md5.md5(password).hexdigest()
                db.session.commit()
            except:
                db.session.rollback()
                flash("Falha ao atualizar o cadastro.", "message-error")
            else:
                flash("Cadastro atualizado com sucesso.", "message-success")

        # Exibe a página
        return redirect("/admin/users")



# --------------------------------------------------------------------------------------------------------------------------------- #

# Página para simulação de instalação de servidores
@app.route("/admin/simulation/install-server", methods=["GET", "POST"])
@login_required
def pageInstallServer():
    user = User.query.filter_by(id_user = current_user.id_user).first()

    # Exibe a página
    if request.method == "GET":

        # Parâmetros da url
        server = request.args.get("servers_simulation", default=0, type=int)
        names_buildings = request.args.get("buildings", default=None, type=str)
        action = request.args.get("action", default=None, type=str)

        # Remover
        if action == "Remover":
            if server == 0:
                flash("Informe o servidor para remover.", "message-error")
                return redirect("/admin/simulation/install-server")
            else:
                server_simulation = ServerSimulation.query.filter_by(id_server_simulation=server).first()
                db.session.delete(server_simulation)
                db.session.flush()
                db.session.commit()
                flash("Servidor removido com sucesso.", "message-success")
                return redirect("/admin/simulation/install-server")


        # Simular
        if action == "Simular":
            if server == 0:
                flash("Informe o servidor para realizar a simulação!", "message-error")
                return redirect("/admin/simulation/install-server")
            return redirect("/admin/simulation/install-server?servers_simulation="+str(server)+"&buildings="+names_buildings)

        # Parâmetros para o site
        list_velocity = EquipamentPortsDao.getListVelocity()
        list_vlan = EquipamentPortsDao.getListVlans()
        servers_simulation = ServerSimulation.query.all()
        buildings = BuildingDao.getBuildingsByUser(user.id_user)

        # Se não for para simular
        if server is None or names_buildings is None:
            return render_template("admin/install_server.html", \
                                    enumerate=enumerate, \
                                    user=user, \
                                    servers_simulation=servers_simulation, \
                                    getClassRange=getClassRange, \
                                    list_velocity=list_velocity, \
                                    list_vlan=list_vlan, \
                                    buildings=buildings,
                                    jsonify=jsonify)

        try:
            # Pega o servidor que deseja-se fazer a simulação
            server_simulation = ServerSimulation.query.filter_by(id_server_simulation=server).first()

            # Pega os racks
            racks = None
            if not names_buildings:
                racks = RacksDetails.query.filter_by(id_user=user.id_user, participate_calculation=1).all()
            else:
                names_buildings_split = names_buildings.split(",")
                racks = RacksDetails.query.filter(RacksDetails.id_user==user.id_user). \
                                           filter(RacksDetails.participate_calculation==1). \
                                           filter(RacksDetails.name_building.in_(names_buildings_split)).all()

            racks_not_participate_calculation = RackDao.getRacksNoParticipateCalculationByUser(user.id_user)
            simulation = installSimulationServer(server_simulation, racks)


            return render_template("admin/install_server.html", \
                                    enumerate=enumerate, \
                                    user=user, \
                                    servers_simulation=servers_simulation, \
                                    server_simulation_selected=server_simulation, \
                                    building_selected=names_buildings, \
                                    list_racks_available=simulation[0], \
                                    list_racks_limiting=simulation[1], \
                                    list_racks_not_participate_calculation=racks_not_participate_calculation, \
                                    getClassRange=getClassRange, \
                                    list_velocity=list_velocity, \
                                    list_vlan=list_vlan, \
                                    buildings=buildings)

        except:
            flash("Não foi possível efetuar a simulação da instalação.", "message-error")
            return redirect("/admin/simulation/install-server")


    # Cadastrar servidor de simulação
    if request.method == "POST":

        action = ""

        try:
            action = request.form['action'].strip()
        except:
            action = request.json['action'].strip()

        if action == "install_server":

            try:
                id_rack = int(request.json['id_rack'].strip())
                id_server = int(request.json['id_server'].strip())

                rack = Rack.query.filter_by(id_rack=id_rack).first()
                server_simulation = ServerSimulation.query.filter_by(id_server_simulation=id_server).first()

                front_ports = []

                for index, port_server_simulation in enumerate(server_simulation.ports):
                    if port_server_simulation.type == 1:
                        # Fibra
                        front_ports.append({"portType": 121002, \
                                            "portStatus": 0, \
                                            "isPending": False, \
                                            "position": (index+1), \
                                            "name": str(index+1), \
                                            "concreteAssetTypeId": 116 })
                    else:
                        # Cobre
                        front_ports.append({"portType": 121001, \
                                            "portStatus": 0, \
                                            "isPending": False, \
                                            "position": (index+1), \
                                            "name": str(index+1), \
                                            "concreteAssetTypeId": 116 })

                server_json = {"name": server_simulation.name, \
                                "concreteAssetTypeId": 68, \
                                "parentId": rack.id_rack_by_api, \
                                "uHeight": server_simulation.us, \
                                "locationInRack": 160001, \
                                "totalPorts": (server_simulation.ports_fiber + server_simulation.ports_copper), \
                                "position": rack.getPositionServerForInstall(server_simulation.us)}

                server = rack.room.floor.building.company.server

                add_server = requests.post(server.api_imvision+"NetworkDevices", \
                                          auth=(server.user_api, server.password_api), \
                                          json=server_json)

                # Servidor adicionado
                if add_server.status_code == 201:

                    server_response = add_server.json()
                    equipament = Equipament()
                    equipament.id_rack = id_rack
                    equipament.name = server_simulation.name
                    equipament.description = ""
                    equipament.potency_watts_usage = 0
                    equipament.electrical_sockets_usage = 0
                    equipament.ru_top = server_response['position']
                    equipament.ru_size = server_simulation.us
                    equipament.ru_buttom = (equipament.ru_top + equipament.ru_size) - 1
                    equipament.resource_type_name = "DeviceinRack"
                    equipament.created_date = datetime.now()
                    equipament.id_equipament_by_api = server_response['id']
                    equipament.id_equipament_parent = None

                    # Adicionar portas
                    for port_json in front_ports:
                        port_json['parentId'] = server_response['id']
                        add_port = requests.post(server.api_imvision+"Ports", \
                                                  auth=(server.user_api, server.password_api), \
                                                  json=port_json)

                        # Porta adicionada
                        if add_port.status_code == 201:

                            port_response = add_port.json()
                            port = EquipamentPorts()
                            port.position = port_response['position']
                            port.name = port_response['name']
                            port.description = port_response['description']
                            port.status = 3
                            port.velocity = None
                            port.service = None
                            port.type = port_response['portTypeName'].upper()
                            port.id_slot_by_api = None
                            port.slot_name = None
                            port.slot_description = None
                            port.id_port_by_api = port_response['id']

                            equipament.equipament_ports.append(port)

                        else:
                            return jsonify({"status": 0, "message": "Servidor adicionado, porém ocorreu falha ao inserir as portas!"})

                    rack.equipaments.append(equipament)
                    db.session.commit()

                    return jsonify({"status": 1, "message": "Servidor adicionado com sucesso!"})

                else:
                    return jsonify({"status": 0, "message": "Não foi possível adicionar o servidor. Code: " + str(add_server.status_code)})


            except:
                db.session.rollback()
                return jsonify({"status": 0, "message": "Não foi possível adicionar o servidor"})


        if action == "simulation":

            try:
                server = int(request.form['server'].strip())
                ports_copper = int(request.form['ports_copper'].strip())
                ports_fiber = int(request.form['ports_fiber'].strip())
                names_buildings = request.form['buildings'].strip()

                server_simulation = ServerSimulation.query.filter_by(id_server_simulation=server).first()
                for port in server_simulation.ports:
                    db.session.delete(port)
                    db.session.flush()

                for x in range(0, ports_fiber):
                    type = 1
                    velocity = request.form['port_fiber_velocity_'+str(x+1)].strip() or None
                    vlan = request.form['port_fiber_vlan_'+str(x+1)].strip() or None
                    port = PortsServerSimulation()
                    port.type = type
                    port.velocity = velocity
                    port.vlan = vlan
                    server_simulation.ports.append(port)

                for x in range(0, ports_copper):
                    type = 2
                    velocity = request.form['port_copper_velocity_'+str(x+1)].strip() or None
                    vlan = request.form['port_copper_vlan_'+str(x+1)].strip() or None
                    port = PortsServerSimulation()
                    port.type = type
                    port.velocity = velocity
                    port.vlan = vlan
                    server_simulation.ports.append(port)

                db.session.commit()
                return redirect("/admin/simulation/install-server?servers_simulation="+str(server_simulation.id_server_simulation)+"&buildings="+names_buildings)

            except:
                db.session.rollback()
                flash("Não foi possível efetuar a simulação da instalação.", "message-error")
                return redirect("/admin/simulation/install-server")


        if action == "add":

            try:

                server_id = request.json['server_id'].strip()
                ports_copper = int(request.json['ports_copper'].strip())
                ports_fiber = int(request.json['ports_fiber'].strip())
                names_buildings = request.json['buildings'].strip()

                # Se deseja utilizar um servidor já cadastrado, aproveitando as novas configurações de portas
                if server_id:
                    try:
                        server_simulation = ServerSimulation.query.filter_by(id_server_simulation=int(server_id)).first()
                        for port in server_simulation.ports:
                            db.session.delete(port)
                            db.session.flush()

                        for x in range(0, ports_fiber):
                            type = 1
                            velocity = request.json['port_fiber_velocity_'+str(x+1)].strip() or None
                            vlan = request.json['port_fiber_vlan_'+str(x+1)].strip() or None
                            port = PortsServerSimulation()
                            port.type = type
                            port.velocity = velocity
                            port.vlan = vlan
                            server_simulation.ports.append(port)

                        for x in range(0, ports_copper):
                            type = 2
                            velocity = request.json['port_copper_velocity_'+str(x+1)].strip() or None
                            vlan = request.json['port_copper_vlan_'+str(x+1)].strip() or None
                            port = PortsServerSimulation()
                            port.type = type
                            port.velocity = velocity
                            port.vlan = vlan
                            server_simulation.ports.append(port)

                        db.session.commit()
                        return jsonify({"status":1, "link":("/admin/simulation/install-server?servers_simulation="+str(server_simulation.id_server_simulation)+"&buildings="+names_buildings)})
                    except:
                        db.session.rollback()
                        return jsonify({"status":2})

                # Tentando cadastrar o novo servidor

                name = request.json['name'].strip()
                us = int(request.json['us'].strip())
                potency_watts = float(request.json['potency_watts'].strip())
                electrical_sockets = int(request.json['electrical_sockets'].strip())

                server_simulation = ServerSimulation()
                server_simulation.name = name
                server_simulation.us = us
                server_simulation.ports_copper = ports_copper
                server_simulation.ports_fiber = ports_fiber
                server_simulation.potency_watts = potency_watts
                server_simulation.electrical_sockets = electrical_sockets

                for x in range(0, ports_fiber):
                    type = 1
                    velocity = request.json['port_fiber_velocity_'+str(x+1)].strip() or None
                    vlan = request.json['port_fiber_vlan_'+str(x+1)].strip() or None
                    port = PortsServerSimulation()
                    port.type = type
                    port.velocity = velocity
                    port.vlan = vlan
                    server_simulation.ports.append(port)

                for x in range(0, ports_copper):
                    type = 2
                    velocity = request.json['port_copper_velocity_'+str(x+1)].strip() or None
                    vlan = request.json['port_copper_vlan_'+str(x+1)].strip() or None
                    port = PortsServerSimulation()
                    port.type = type
                    port.velocity = velocity
                    port.vlan = vlan
                    server_simulation.ports.append(port)

                # Verifica se o servidor já esta cadastrado
                servers_simulation = ServerSimulation.query.all()

                for server in servers_simulation:
                    if server.us == server_simulation.us and server.ports_copper == server_simulation.ports_copper and server.ports_fiber == server_simulation.ports_fiber and server.potency_watts == server_simulation.potency_watts and server.electrical_sockets == server_simulation.electrical_sockets:
                        return jsonify({"status":2, "server_name": server.name, "server_id": server.id_server_simulation})

                db.session.add(server_simulation)
                db.session.commit()

                return jsonify({"status":1, "link":("/admin/simulation/install-server?servers_simulation="+str(server_simulation.id_server_simulation)+"&buildings="+names_buildings)})

            except:
                db.session.rollback()
                return jsonify({"status":3})


# --------------------------------------------------------------------------------------------------------------------------------- #

# Página para simulação de instalação de servidores
@app.route("/admin/general", methods=["GET", "POST"])
@login_required
def pageGeneral():
    user = User.query.filter_by(id_user = current_user.id_user).first()
    racks = RackDao.getRacksByUser(user.id_user)
    group_connections = GroupConnection.query.all()
    electrical_parameters = ElectricalParameters.query.first()

    # Exibe a página
    if request.method == "GET":
        buildings = BuildingDao.getBuildingsByUser(user.id_user)
        servers_simulation = ServerSimulation.query.all()
        return render_template("admin/general.html", \
                                enumerate=enumerate, \
                                user=user, \
                                racks=racks, \
                                group_connections=group_connections, \
                                buildings=buildings, \
                                electrical_parameters=electrical_parameters)

    if request.method == "POST":
        action = request.json['action'].strip()

        if action == "rack_participate":
            try:
                id = int(request.json['id'].strip())
                participate = request.json['participate']
                rack = Rack.query.filter_by(id_rack=id).first()
                rack.participate_calculation = participate
                db.session.commit()
            except:
                db.session.rollback()
                return jsonify({"status": 0})
            else:
                return jsonify({"status": 1})

        if action == "add_group_connection":
            try:
                group_connection = GroupConnection()
                name = request.json['name'].strip()
                group_connection.name = name
                ids_racks = request.json['racks'].strip()
                racks = []

                if ids_racks:
                    ids = ids_racks.split(",")
                    for id in ids:
                        rack = Rack.query.filter_by(id_rack=(int(id.strip()))).first()
                        group_connection.racks.append(rack)
                        racks.append({"id": rack.id_rack, "name": rack.name})

                GroupConnectionDao.insertGroupConnection(group_connection)
                db.session.commit()

                data = {"status": 1, "data": {"name": name, "id": group_connection.id_group_connection, "racks": racks, "racks_string": group_connection.getIdsRacksToString()}}
                return jsonify(data)

            except:
                db.session.rollback()
                return jsonify({"status": 0})

        if action == "update_group_connection":
            try:

                name = request.json['name'].strip()
                id = request.json['id'].strip()

                group_connection = GroupConnection.query.filter_by(id_group_connection=id).first()
                group_connection.name = name

                for group_rack_connection in group_connection.group_rack_connections:
                    db.session.delete(group_rack_connection)
                    db.session.flush()

                ids_racks = request.json['racks'].strip()
                racks = []

                if ids_racks:
                    ids = ids_racks.split(",")
                    for id in ids:
                        rack = Rack.query.filter_by(id_rack=(int(id.strip()))).first()
                        group_connection.racks.append(rack)
                        db.session.flush()
                        racks.append({"id": rack.id_rack, "name": rack.name})

                db.session.commit()
                data = {"status": 1, "data": {"name": name, "id": group_connection.id_group_connection, "racks": racks, "racks_string": group_connection.getIdsRacksToString()}}
                return jsonify(data)

            except:
                db.session.rollback()
                return jsonify({"status": 0})

        if action == "remove_group_connection":
            id = int(request.json['id'].strip())
            try:
                GroupConnectionDao.removeGroupConnection(id)
                db.session.commit()
            except:
                db.session.rollback()
                return jsonify({"status": 0})
            else:
                return jsonify({"status": 1})

        if action == "update_electrical_parameters":
            try:
                potency_indice = float(request.json['potency_indice']);
                electrical_parameters = ElectricalParameters.query.first()
                electrical_parameters.potency_indice = potency_indice
                db.session.commit()
            except:
                db.session.rollback()
                return jsonify({"status": 0})
            else:
                return jsonify({"status": 1})






# --------------------------------------------------------------------------------------------------------------------------------- #


def getResultsSimulation(id_user, names_buildings, ids_servers):
    servers = []
    for id_server in ids_servers:
        servers.append(ServerSimulation.query.filter_by(id_server_simulation=int(id_server.strip())).first())

    racks = []

    if not names_buildings:
        racks = RacksDetails.query.filter_by(id_user=id_user, participate_calculation=1).all()
    else:
        names_buildings_split = names_buildings.split(",")
        racks = RacksDetails.query.filter(RacksDetails.id_user==id_user).filter(RacksDetails.participate_calculation==1).filter(RacksDetails.name_building.in_(names_buildings_split)).all()

    list = []

    for rack in racks:
        simulation_future = SimulationFuture(rack, servers)
        result = simulation_future.simulation()
        if result is not None:
            result.append(servers)
            list.append(result)

    return list

@app.route("/admin/simulation/future", methods=["GET", "POST"])
def pageSimulationFuture():
    user = User.query.filter_by(id_user = current_user.id_user).first()

    # Exibe a página
    if request.method == "GET":

        # Parâmetros da url
        servers = request.args.get("servers_simulation", default=None, type=str)
        names_buildings = request.args.get("buildings", default=None, type=str)

        # Parâmetros para o site
        list_velocity = EquipamentPortsDao.getListVelocity()
        list_vlan = EquipamentPortsDao.getListVlans()
        servers_simulation = ServerSimulation.query.all()
        buildings = BuildingDao.getBuildingsByUser(user.id_user)

        if servers is None:
            return render_template("admin/simulation_future.html", \
                                    enumerate=enumerate, \
                                    user=user, \
                                    servers_simulation=servers_simulation, \
                                    getClassRange=getClassRange, \
                                    list_velocity=list_velocity, \
                                    list_vlan=list_vlan, \
                                    buildings=buildings, \
                                    jsonify=jsonify)

        # Resultados da simulação
        list = getResultsSimulation(user.id_user, names_buildings, servers.split(","))
        racks_not_participate_calculation = RackDao.getRacksNoParticipateCalculationByUser(user.id_user)

        return render_template("admin/simulation_future.html", \
                                enumerate=enumerate, \
                                user=user, \
                                servers_simulation=servers_simulation, \
                                getClassRange=getClassRange, \
                                list_velocity=list_velocity, \
                                list_vlan=list_vlan, \
                                buildings=buildings, \
                                jsonify=jsonify, \
                                servers_simulation_selected=servers, \
                                building_selected=names_buildings, \
                                list_racks_available=list, \
                                list_racks_not_participate_calculation=racks_not_participate_calculation)

    if request.method == "POST":

        action = request.form['action'].strip()

        if action == "simulation":

            try:

                servers = request.form['servers'].strip()
                names_buildings = request.form['buildings'].strip()

                for server in servers.split(","):
                    server_simulation = ServerSimulation.query.filter_by(id_server_simulation=int(server)).first()
                    for port in server_simulation.ports:
                        db.session.delete(port)
                        db.session.flush()

                    ports_fiber = int(request.form['ports_fiber_serverid_'+server].strip())
                    ports_copper = int(request.form['ports_copper_serverid_'+server].strip())

                    for x in range(0, ports_fiber):
                        type = 1
                        velocity = request.form['port_fiber_velocity_'+str(x+1)+'_serverid_'+server].strip() or None
                        vlan = request.form['port_fiber_vlan_'+str(x+1)+'_serverid_'+server].strip() or None
                        port = PortsServerSimulation()
                        port.type = type
                        port.velocity = velocity
                        port.vlan = vlan
                        server_simulation.ports.append(port)

                    for x in range(0, ports_copper):
                        type = 2
                        velocity = request.form['port_copper_velocity_'+str(x+1)+'_serverid_'+server].strip() or None
                        vlan = request.form['port_copper_vlan_'+str(x+1)+'_serverid_'+server].strip() or None
                        port = PortsServerSimulation()
                        port.type = type
                        port.velocity = velocity
                        port.vlan = vlan
                        server_simulation.ports.append(port)

                db.session.commit()
                return redirect("/admin/simulation/future?servers_simulation="+servers+"&buildings="+names_buildings)

            except:
                db.session.rollback()
                flash("Não foi possível efetuar a simulação.", "message-error")
                return redirect("/admin/simulation/future")




# --------------------------------------------------------------------------------------------------------------------------------- #


# Busca atualizações pendentes
def dataUpdateLast(server):
    data = None
    datetime_format = '%Y-%m-%d %H:%M:%S.%f'
    # Data de inicio da atualização
    date_start = datetime.now()

    try:
        response_json = requests.get(server.url + API_DATA_UPDATE_LAST + "?date-last="+server.last_update_date.strftime('%Y-%m-%d %H:%M:%S') + \
        "&ip-database="+server.ip_database + "&user-database="+server.user_database + "&password-database="+server.password_database).json()

        for data_json in response_json:
            if data_json['type'] == "company":
                CompanyDao.updateByAPI(server, data_json)
            if data_json['type'] == "building":
                BuildingDao.updateByAPI(server, data_json)
            if data_json['type'] == "floor":
                FloorDao.updateByAPI(server, data_json)
            if data_json['type'] == "room":
                RoomDao.updateByAPI(server, data_json)
            if data_json['type'] == "rack group":
                GroupRackDao.updateByAPI(server, data_json)
            if data_json['type'] == "rack":
                RackDao.updateByAPI(server, data_json)
            if data_json['type'] == "device":
                EquipamentDao.updateByAPI(server, data_json)
            if data_json['type'] == "port":
                EquipamentPortsDao.updateByAPI(server, data_json)
            if data_json['type'] == "slot" and data_json['action'] != 2:
                SlotDao.updateByAPI(server, data_json)

        date_final = datetime.now()
        seconds = getDifferenceSeconds(datetime.strptime(str(date_start), datetime_format), datetime.strptime(str(date_final), datetime_format))
        server.last_update_date = date_final
        db.session.commit()

    # fim try
    except:
        date_final = datetime.now()
        seconds = getDifferenceSeconds(datetime.strptime(str(date_start), datetime_format),
                                       datetime.strptime(str(date_final), datetime_format))
        data = {"server": server.url, "message": MESSAGE_ERROR_DATA_UPDATE_API, "time_update_seconds": seconds}
        db.session.rollback()

    else:
        data = {"server": server.url, "message": MESSAGE_SUCCESS_DATA_UPDATE_API, "time_update_seconds": seconds}

    return data



# --------------------------------------------------------------------------------------------------------------------------------- #



# Faz uma atualização completa
def dataUpdateAll(server):
    data = None
    datetime_format = '%Y-%m-%d %H:%M:%S.%f'
    # Data de inicio da atualização
    date_start = datetime.now()

    try:

        for company in server.companies:
            db.session.delete(company)
            db.session.flush()

        #data_json = json.loads(open('/var/www/TsinovaApp/TsinovaApp/controller/response.json').read())
        data_json = requests.get(server.url + API_DATA_UPDATE_ALL + "?ip-database="+server.ip_database + "&user-database="+server.user_database + "&password-database="+server.password_database).json()

        for company_json in data_json['companies']:
            company = Company();
            company.id_server = server.id_server
            company.name = company_json['name'] or ""
            company.description = company_json['description'] or ""
            company.id_company_by_api = company_json['id']
            server.companies.append(company)
            db.session.flush()

            for building_json in company_json['buildings']:
                building = Building()
                building.name = building_json['name']
                building.description = building_json['description']
                building.id_building_by_api = building_json['id']
                company.buildings.append(building)
                db.session.flush()

                for floor_json in building_json['floors']:
                    floor = Floor()
                    floor.number = floor_json['number']
                    floor.name = floor_json['name']
                    floor.description = floor_json['description']
                    floor.id_floor_by_api = floor_json['id']
                    building.floors.append(floor)
                    db.session.flush()

                    for room_json in floor_json['rooms']:
                        room = Room()
                        room.name = room_json['name']
                        room.description = room_json['description']
                        room.id_room_by_api = room_json['id']

                        for group_rack_json in room_json['group_racks']:
                            group_rack = GroupRack()
                            group_rack.name = group_rack_json['name']
                            group_rack.description = group_rack_json['description']
                            group_rack.id_group_rack_by_api = group_rack_json['id']
                            room.group_racks.append(group_rack)

                        floor.rooms.append(room)
                        db.session.flush()

                        for rack_json in room_json['racks']:
                            rack = Rack()
                            rack.name = rack_json['name']
                            rack.description = rack_json['description']
                            rack.zone = rack_json['zone']
                            rack.position_zone = rack_json['position_zone']
                            rack.total_ru = rack_json['total_ru']
                            rack.potency_watts_limit = rack_json['potency_watts_limit']
                            rack.electrical_sockets_limit = rack_json['electrical_sockets_limit']
                            rack.id_rack_by_api = rack_json['id']
                            rack.created_date = datetime.strptime(rack_json['created_date']['date'], '%Y-%m-%d %H:%M:%S.%f')
                            rack.participate_calculation_fiber_ports = True

                            if rack_json['group_rack'] is not None:
                                for group_rack in room.group_racks:
                                    if group_rack.id_group_rack_by_api == rack_json['group_rack']['id']:
                                        rack.group_rack = group_rack
                                        break

                            room.racks.append(rack)
                            db.session.flush()

                            for equipament_json in rack_json['equipaments']:
                                equipament = Equipament()
                                equipament.name = equipament_json['name']
                                equipament.description = equipament_json['description']
                                equipament.ru_size = equipament_json['ru_size']
                                equipament.ru_top = equipament_json['ru_top']
                                equipament.ru_buttom = equipament_json['ru_buttom']
                                equipament.id_equipament_by_api = equipament_json['id']
                                equipament.potency_watts_usage = equipament_json['potency_watts_usage']
                                equipament.electrical_sockets_usage = equipament_json['electrical_sockets_usage']
                                equipament.created_date = datetime.strptime(equipament_json['created_date']['date'], '%Y-%m-%d %H:%M:%S.%f')
                                equipament.resource_type_name = equipament_json['resource_type_name']

                                if "id_equipament_parent" in equipament_json:
                                    for obj in rack.equipaments:
                                        if obj.id_equipament_by_api == equipament_json['id_equipament_parent']:
                                            equipament.id_equipament_parent = obj.id_equipament
                                            break

                                for equipament_port_json in data_json['ports'][:]:
                                    if equipament.id_equipament_by_api != equipament_port_json['id']:
                                        continue
                                    for port_json in equipament_port_json['ports']:
                                        equipament_port = EquipamentPorts()
                                        equipament_port.position = port_json['position']
                                        equipament_port.name = port_json['name']
                                        equipament_port.description = port_json['description']
                                        equipament_port.status = port_json['status']
                                        equipament_port.service = port_json['service']
                                        equipament_port.velocity = port_json['velocity']
                                        equipament_port.type = port_json['type_port']
                                        equipament_port.id_slot_by_api = port_json['id_slot_by_api']
                                        equipament_port.slot_name = port_json['slot_name']
                                        equipament_port.slot_description = port_json['slot_description']
                                        equipament_port.id_port_by_api = port_json['id']
                                        equipament.equipament_ports.append(equipament_port)
                                    data_json['ports'].remove(equipament_port_json)

                                rack.equipaments.append(equipament)
                                db.session.flush()

                            # fim for equipament_json
                        # fim for rak_json
                    # fim for room_json
                # fim for floor_json
            # fim for building_json
        # fim for company_json

        date_final = datetime.now()
        seconds = getDifferenceSeconds(datetime.strptime(str(date_start), datetime_format), datetime.strptime(str(date_final), datetime_format))
        server.last_update_date = date_final
        db.session.commit()

    # fim try
    except:
        date_final = datetime.now()
        seconds = getDifferenceSeconds(datetime.strptime(str(date_start), datetime_format),
                                       datetime.strptime(str(date_final), datetime_format))
        data = {"server": server.url, "message": MESSAGE_ERROR_DATA_UPDATE_API, "time_update_seconds": seconds}
        db.session.rollback()

    else:
        data = {"server": server.url, "message": MESSAGE_SUCCESS_DATA_UPDATE_API, "time_update_seconds": seconds}

    return data


# --------------------------------------------------------------------------------------------------------------------------------- #

def installSimulationServer(server, racks):

    list_racks_available = [] # Lista de racks disponíveis para a instalação
    list_racks_limiting = [] # Racks com limitações

    for rack in racks:

        new_rack = deepcopy(rack)

        set_us = new_rack.setUsUse(server.us)

        #set_potency = new_rack.setPotencyUse(server.potency_watts)
        set_potency = new_rack.setPotencyUse(server.getPotencyWatts())

        set_electrical = new_rack.setElectricalUse(server.electrical_sockets)
        set_ports = new_rack.setPortsUse(server)
        set_ports_copper = set_ports[0]
        set_ports_fiber = set_ports[1]

        # Racks que não possuem limitações quanto a instalação do novo servidor
        if set_us and set_potency and set_electrical and set_ports_copper and set_ports_fiber:
            list_racks_available.append([rack, new_rack])
            continue

        # Racks com limitações para a instalação do novo servidor
        rack.limiting = Limiting()
        if not set_us:
            rack.limiting.us = True
        if not set_potency:
            rack.limiting.potency = True
        if not set_electrical:
            rack.limiting.electrical = True
        if not set_ports_copper:
            rack.limiting.ports_copper = True
        if not set_ports_fiber:
            rack.limiting.ports_fiber = True
        list_racks_limiting.append(rack)

    return [list_racks_available, list_racks_limiting]



# --------------------------------------------------------------------------------------------------------------------------------- #



# Desconecta o usuário da plataforma - Sair
@app.route("/admin/logout")
@login_required
def logout():
    logout_user()
    return redirect("/")


# --------------------------------------------------------------------------------------------------------------------------------- #


# Carrega o usuário
@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


# --------------------------------------------------------------------------------------------------------------------------------- #


# Redireciona o usuário paga a página de login caso não conectado
@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect("/")
