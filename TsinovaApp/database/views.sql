USE sysanalytical;

DROP VIEW ports_details;
CREATE VIEW ports_details AS
SELECT room.name AS name_room,
	   room.id_room,
	   rack.name AS name_rack,
       rack.id_rack,
       rack.zone,
       equipament.name AS name_equipament,
       equipament.id_equipament,
       equipament.resource_type_name,
       equipament_port.name AS name_port,
       equipament_port.id_equipament_port,
       equipament_port.position,
       equipament_port.type,
       equipament_port.velocity,
       equipament_port.service,
       equipament_port.status
FROM room AS room
INNER JOIN rack AS rack ON rack.id_room = room.id_room
INNER JOIN equipament AS equipament ON equipament.id_rack = rack.id_rack
INNER JOIN equipament_ports AS equipament_port ON equipament_port.id_equipament = equipament.id_equipament;


DROP VIEW ports_rack;
CREATE VIEW ports_rack AS SELECT
rack.id_rack,
equipament.resource_type_name,
equipament_ports.id_equipament_port,
equipament_ports.name,
equipament_ports.position,
equipament_ports.velocity,
equipament_ports.service,
equipament_ports.type,
equipament_ports.status
FROM rack AS rack
INNER JOIN equipament AS equipament ON equipament.id_rack = rack.id_rack
INNER JOIN equipament_ports AS equipament_ports ON equipament_ports.id_equipament = equipament.id_equipament;


DROP VIEW racks_ports_switch_connect;
CREATE VIEW racks_ports_switch_connect AS SELECT
group_rack_connection_a.id_rack,
IFNULL(CONCAT("[", GROUP_CONCAT(  (CASE WHEN (ports_rack.status != 2 AND ports_rack.resource_type_name = "NetworkEquipment") THEN JSON_OBJECT("id_rack", ports_rack.id_rack, "resource_type_name", ports_rack.resource_type_name, "velocity", ports_rack.velocity, "service", ports_rack.service, "type", ports_rack.type, "status", ports_rack.status) END)  ), "]"), "[]") AS ports_switch_available_connection
FROM group_rack_connection AS group_rack_connection_a
INNER JOIN group_rack_connection AS group_rack_connection_b ON group_rack_connection_b.id_group_connection = group_rack_connection_a.id_group_connection
INNER JOIN ports_rack AS ports_rack ON ports_rack.id_rack = group_rack_connection_b.id_rack
WHERE group_rack_connection_a.id_rack != group_rack_connection_b.id_rack
GROUP BY group_rack_connection_a.id_rack
ORDER BY group_rack_connection_a.id_rack;


DROP VIEW zone_ports_used;
CREATE VIEW zone_ports_used AS SELECT ports_details.id_room, ports_details.zone,
SUM(CASE WHEN (ports_details.resource_type_name = "PatchPanel" AND ports_details.type != "RJ-45" AND ports_details.status = 1) THEN 1 ELSE 0 END) qtd_ports_fiber_available,
SUM(CASE WHEN (ports_details.resource_type_name = "PatchPanel" AND ports_details.type != "RJ-45" AND ports_details.status = 2) THEN 1 ELSE 0 END) qtd_ports_fiber_used,
SUM(CASE WHEN (ports_details.resource_type_name = "NetworkEquipment" AND ports_details.status != 2) THEN 1 ELSE 0 END) qtd_ports_switch_available,
SUM(CASE WHEN (ports_details.resource_type_name = "NetworkEquipment" AND ports_details.status = 2) THEN 1 ELSE 0 END) qtd_ports_switch_used,
SUM(CASE WHEN (ports_details.resource_type_name = "NetworkEquipment" AND ports_details.status != 2 AND ports_details.type = "RJ-45") THEN 1 ELSE 0 END) qtd_ports_switch_copper_available,
SUM(CASE WHEN (ports_details.resource_type_name = "NetworkEquipment" AND ports_details.status != 2 AND ports_details.type != "RJ-45") THEN 1 ELSE 0 END) qtd_ports_switch_fiber_available,
CONCAT("[", GROUP_CONCAT(  (CASE WHEN (ports_details.status != 2 AND ports_details.resource_type_name = "NetworkEquipment") THEN JSON_OBJECT("velocity", ports_details.velocity, "service", ports_details.service, "type", ports_details.type, "status", ports_details.status, "resource_type_name", ports_details.resource_type_name) END)  ), "]") AS ports_switch_available
FROM ports_details AS ports_details
GROUP BY ports_details.id_room, ports_details.zone;


drop view racks_ports_copper;
CREATE VIEW racks_ports_copper AS SELECT ports_details.id_rack,
SUM(CASE WHEN (ports_details.resource_type_name = "PatchPanel" AND ports_details.type = "RJ-45" AND ports_details.status = 1) THEN 1 ELSE 0 END) qtd_ports_copper_available,
SUM(CASE WHEN (ports_details.resource_type_name = "PatchPanel" AND ports_details.type = "RJ-45" AND ports_details.status = 2) THEN 1 ELSE 0 END) qtd_ports_copper_used
FROM ports_details AS ports_details
GROUP BY ports_details.id_rack;


DROP VIEW racks_ports_switch_connect_zone;
CREATE VIEW racks_ports_switch_connect_zone AS SELECT
rack.id_rack,
IFNULL(CONCAT("[", GROUP_CONCAT( JSON_OBJECT("id_rack", racks_ports_switch_connect.id_rack, "ports", IFNULL(racks_ports_switch_connect.ports_switch_available_connection, "[]")) ), "]"), "[]") AS ports_switch_available_connection
FROM rack AS rack
INNER JOIN rack as rack_b ON rack_b.id_room = rack.id_room and rack_b.zone = rack.zone
LEFT JOIN racks_ports_switch_connect ON racks_ports_switch_connect.id_rack = rack_b.id_rack
GROUP BY rack.id_rack;


DROP VIEW racks_details;
create view racks_details as SELECT
server_user.id_user,
server.id_server,
company.id_company,
building.id_building,
building.name AS name_building,
floor.id_floor,
room.id_room,
rack.id_rack,
rack.name AS name_rack,
CONCAT(company.name, "/", building.name, "/", floor.name, "/",
room.name, "/",  if(isnull(rack.id_group_rack), "", CONCAT(group_rack.name, "/"))  , "Fileira: ", rack.zone, ", Posição: ",
rack.position_zone) AS local_installation,
rack.zone,
rack.position_zone,
rack.total_ru,
rack.participate_calculation,
rack.potency_watts_limit,
rack.electrical_sockets_limit,
(SELECT IFNULL(SUM(equipament.ru_size),0) FROM equipament AS equipament WHERE equipament.id_rack = rack.id_rack) AS ru_usage,
(SELECT IFNULL(SUM(equipament.potency_watts_usage),0) FROM equipament AS equipament WHERE equipament.id_rack = rack.id_rack) AS potency_watts_usage,
(SELECT IFNULL(SUM(equipament.electrical_sockets_usage),0) FROM equipament AS equipament WHERE equipament.id_rack = rack.id_rack) AS electrical_sockets_usage,
IFNULL(racks_ports_copper.qtd_ports_copper_available,0) AS qtd_ports_copper_available,
IFNULL(racks_ports_copper.qtd_ports_copper_used,0) AS qtd_ports_copper_used,
IFNULL(zone_ports_used.qtd_ports_fiber_available,0) AS qtd_ports_fiber_available,
IFNULL(zone_ports_used.qtd_ports_fiber_used,0) AS qtd_ports_fiber_used,
IFNULL(zone_ports_used.qtd_ports_switch_available,0) AS qtd_ports_switch_available,
IFNULL(zone_ports_used.qtd_ports_switch_used,0) AS qtd_ports_switch_used,
IFNULL(zone_ports_used.qtd_ports_switch_copper_available,0) AS qtd_ports_switch_copper_available,
IFNULL(zone_ports_used.qtd_ports_switch_fiber_available,0) AS qtd_ports_switch_fiber_available,
IFNULL(zone_ports_used.ports_switch_available,"[]") AS ports_switch_available,
racks_ports_switch_connect_zone.ports_switch_available_connection
FROM rack AS rack
LEFT JOIN group_rack AS group_rack ON group_rack.id_group_rack = rack.id_group_rack
INNER JOIN room AS room ON room.id_room = rack.id_room
INNER JOIN floor AS floor ON floor.id_floor = room.id_floor
INNER JOIN building AS building ON building.id_building = floor.id_building
INNER JOIN company AS company ON company.id_company = building.id_company
INNER JOIN server AS server ON server.id_server = company.id_server
INNER JOIN server_user AS server_user ON server_user.id_server = server.id_server
INNER JOIN racks_ports_switch_connect_zone ON racks_ports_switch_connect_zone.id_rack = rack.id_rack
LEFT JOIN zone_ports_used AS zone_ports_used ON zone_ports_used.id_room = rack.id_room AND zone_ports_used.zone = rack.zone
LEFT JOIN racks_ports_copper AS racks_ports_copper ON racks_ports_copper.id_rack = rack.id_rack
GROUP BY server_user.id_user, rack.id_rack;


CREATE VIEW building_details_ports AS
SELECT
server_user.id_user,
building.id_building,
building.name AS name_building,
SUM(CASE WHEN (equipament.resource_type_name = "PatchPanel" AND equipament_ports.type != "RJ-45" AND equipament_ports.status = 1) THEN 1 ELSE 0 END) qtd_ports_fiber_available,
SUM(CASE WHEN (equipament.resource_type_name = "PatchPanel" AND equipament_ports.type != "RJ-45" AND equipament_ports.status = 2) THEN 1 ELSE 0 END) qtd_ports_fiber_used,
SUM(CASE WHEN (equipament.resource_type_name = "PatchPanel" AND equipament_ports.type = "RJ-45" AND equipament_ports.status = 1) THEN 1 ELSE 0 END) qtd_ports_copper_available,
SUM(CASE WHEN (equipament.resource_type_name = "PatchPanel" AND equipament_ports.type = "RJ-45" AND equipament_ports.status = 2) THEN 1 ELSE 0 END) qtd_ports_copper_used,
SUM(CASE WHEN (equipament.resource_type_name = "NetworkEquipment" AND equipament_ports.status != 2) THEN 1 ELSE 0 END) qtd_ports_switch_available,
SUM(CASE WHEN (equipament.resource_type_name = "NetworkEquipment" AND equipament_ports.status = 2) THEN 1 ELSE 0 END) qtd_ports_switch_used
FROM building
LEFT JOIN floor ON floor.id_building = building.id_building
LEFT JOIN room ON room.id_floor = floor.id_floor
LEFT JOIN rack ON rack.id_room = room.id_room
LEFT JOIN equipament ON equipament.id_rack = rack.id_rack
LEFT JOIN equipament_ports ON equipament_ports.id_equipament = equipament.id_equipament
LEFT JOIN company ON company.id_company = building.id_company
LEFT JOIN server ON server.id_server = company.id_server
LEFT JOIN server_user ON server_user.id_server = server.id_server
GROUP BY server_user.id_user, building.id_building

DROP VIEW building_details;
CREATE VIEW building_details AS
SELECT
server_user.id_user,
building.id_building,
building.name AS name_building,
SUM(CASE WHEN (equipament.resource_type_name = "NetworkEquipment") THEN 1 ELSE 0 END) qtd_switchs,
SUM(CASE WHEN (equipament.resource_type_name = "PatchPanel") THEN 1 ELSE 0 END) qtd_pathpanels,
SUM(CASE WHEN (equipament.resource_type_name = "DeviceinRack") THEN 1 ELSE 0 END) qtd_servers,
SUM(CASE WHEN (equipament.resource_type_name = "BladeEnclosure") THEN 1 ELSE 0 END) qtd_blades,
SUM(CASE WHEN (equipament.resource_type_name = "BladeServer") THEN 1 ELSE 0 END) qtd_servers_blades,
(SELECT COUNT(*) FROM rack AS ra
LEFT JOIN room AS ro ON ro.id_room = ra.id_room
LEFT JOIN floor AS flo ON flo.id_floor = ro.id_floor
WHERE flo.id_building = building.id_building) AS qtd_racks,
building_details_ports.qtd_ports_copper_available,
building_details_ports.qtd_ports_copper_used,
building_details_ports.qtd_ports_fiber_available,
building_details_ports.qtd_ports_fiber_used,
building_details_ports.qtd_ports_switch_available,
building_details_ports.qtd_ports_switch_used
FROM building
LEFT JOIN floor ON floor.id_building = building.id_building
LEFT JOIN room ON room.id_floor = floor.id_floor
LEFT JOIN rack ON rack.id_room = room.id_room
LEFT JOIN equipament ON equipament.id_rack = rack.id_rack
LEFT JOIN company ON company.id_company = building.id_company
LEFT JOIN server ON server.id_server = company.id_server
LEFT JOIN server_user ON server_user.id_server = server.id_server
INNER JOIN building_details_ports ON building_details_ports.id_building = building.id_building AND building_details_ports.id_user = server_user.id_user
GROUP BY server_user.id_user, building.id_building
