from TsinovaApp import db

class ZonePortsUsed(db.Model):
    __tablename__ = "zone_ports_used"

    id_room = db.Column("id_room", db.Integer, primary_key=True)
    zone = db.Column("zone", db.Integer, primary_key=True)
    qtd_ports_copper_available = db.Column("qtd_ports_copper_available", db.Integer)
    qtd_ports_copper_used = db.Column("qtd_ports_copper_used", db.Integer)
    qtd_ports_fiber_available = db.Column("qtd_ports_fiber_available", db.Integer)
    qtd_ports_fiber_used = db.Column("qtd_ports_fiber_used", db.Integer)
    qtd_ports_switch_available = db.Column("qtd_ports_switch_available", db.Integer)
    qtd_ports_switch_used = db.Column("qtd_ports_switch_used", db.Integer)

    def __init__(self):
        pass
