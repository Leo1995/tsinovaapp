from TsinovaApp import db
import server

class UpdateServerLog(db.Model):
    __tablename__ = "update_server_log"

    id_update_server_log = db.Column("id_update_server_log", db.Integer, primary_key=True, autoincrement=True)
    id_server = db.Column("id_server", db.Integer, db.ForeignKey("server.id_server"))
    date_update = db.Column("date_update", db.DateTime, nullable=False)
    message = db.Column("message", db.String(250), nullable=False)

    server = db.relationship("Server", uselist=False, backref="update_server_log")

    def __init__(self, id_server=None, date_update=None, message=""):
        self.id_server = id_server
        self.message = message
        self.date_update = date_update

    def __repr__(self):
        return '<UpdateServerLog %r>' % self.message
