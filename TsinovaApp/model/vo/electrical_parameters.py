from TsinovaApp import db

class ElectricalParameters(db.Model):
    __tablename__ = "electrical_parameters"

    id_electrical_parameter = db.Column("id_electrical_parameter", db.Integer, primary_key=True, autoincrement=True)
    potency_indice = db.Column("potency_indice", db.Float, nullable=False)

    def __init__(self):
        pass
