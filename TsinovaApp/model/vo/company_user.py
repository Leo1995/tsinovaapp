from TsinovaApp import db
import user
import company

class CompanyUser(db.Model):
    __tablename__ = "company_user"
    __table_args__ = {'extend_existing': True}

    id_user = db.Column("id_user", db.Integer, db.ForeignKey("user.id_user"), primary_key=True)
    user = db.relationship("User", uselist=False, backref="company_user")
    id_company = db.Column("id_company", db.Integer, db.ForeignKey("company.id_company"), primary_key=True)
    company = db.relationship("Company", uselist=False, backref="company_user")

    def __init__(self, id_user=None, id_company=None):
        self.id_user = id_user
        self.id_company = id_company

    def __repr__(self):
        return '<CompanyUser %r>' % self.id_user
