# -*- Coding: UTF-8 -*-
#coding: utf-8

from equipament import Equipament
from racks_details import RacksDetails
from server_simulation import ServerSimulation
from copy import copy, deepcopy

class SimulationFuture():

    def __init__(self, rack, servers):
        self.rack = rack
        self.servers = servers

        matriz_resources_available_rack = self.getMatrizResourcesAvailableFromRack(self.rack)
        max_instalations = []

        for server in self.servers:
            matriz_simulation_server = self.getMatrizSimulationFromServer(server, matriz_resources_available_rack)
            max_instalations.append(min(matriz_simulation_server))

        self.probabilities = self.calculateProbability(max_instalations, 0, [])
        self.probabilities = self.clearInvalidProbabilities(max(max_instalations), self.probabilities)
        self.probabilities = self.orderProbability(self.probabilities)

        self.rack.loadEquipaments()
        self.rack.loadPortsSwitchAvailableZoneByJson()
        self.rack.loadPortsSwitchAvailableGroupConnectionByJson()


    # Ordena as probabilidades em ordem descrecente de número máximo de servidores
    def orderProbability(self, v):
        for i in range(len(v), 0, -1):
            for j in range(1, i):
                if sum(v[j-1]) < sum(v[j]):
                    aux = v[j]
                    v[j] = v[j-1]
                    v[j-1] = aux
        return v

    # Ordena as probabilidades em ordem descrecente de número máximo de servidores
    def orderResults(self, v):
        for i in range(len(v), 0, -1):
            for j in range(1, i):
                if v[j-1][2].getStandardDeviation() > v[j][2].getStandardDeviation():
                    aux = v[j]
                    v[j] = v[j-1]
                    v[j-1] = aux
        return v


    # Efetua a simulação para cada probabilidade
    def simulation(self):

        servers = self.servers
        probabilities = self.probabilities
        rack = self.rack

        list = []

        for index, probability in enumerate(probabilities):

            # nova função
            if len(list) > 0:
                if sum(list[0][0]) > sum(probability):
                    break

            # É possível executar a probabilidade com a tomada e potência que tenho ?
            potency_watts_usage = 0
            electrical_sockets_usage = 0
            for index, server in enumerate(servers):
                potency_watts_usage += server.getPotencyWatts() * probability[index]
                electrical_sockets_usage += server.electrical_sockets * probability[index]
            if potency_watts_usage > (rack.potency_watts_limit - rack.potency_watts_usage) or electrical_sockets_usage > (rack.electrical_sockets_limit - rack.electrical_sockets_usage):
                continue

            # É possível executar a probabilidade com os Us que tenho ?
            # Passo 1
            total_us_usage = 0
            for index, server in enumerate(servers):
                total_us_usage += server.us * probability[index]
            if total_us_usage > (rack.total_ru - rack.ru_usage):
                continue
            # Passo 2
            equipaments = deepcopy(rack.equipaments)
            load_table_us = self.loadTableUs(rack.total_ru, equipaments)
            flag = True
            for index, server in enumerate(servers):
                i = 1
                while(i <= probability[index]):
                    flag = self.addServerUs(load_table_us, equipaments, server)
                    if not flag:
                        break
                    i = i + 1
                if not flag:
                    break
            if not flag:
                continue

            # É possível executar a probabilidade com as portas que tenho?
            # Passo 1
            total_ports_copper_usage = 0
            total_ports_fiber_usage = 0
            for index, server in enumerate(servers):
                total_ports_copper_usage += server.ports_copper * probability[index]
                total_ports_fiber_usage += server.ports_fiber * probability[index]
            if total_ports_copper_usage > (rack.qtd_ports_copper_available + rack.qtd_ports_switch_copper_available) or total_ports_fiber_usage > (rack.qtd_ports_fiber_available + rack.qtd_ports_switch_fiber_available):
                continue
            # Passo 2
            rack_simulation = deepcopy(rack)
            flag = True
            for index, server in enumerate(servers):
                i = 1
                while(i <= probability[index]):
                    flag = self.setPortsUse(rack_simulation, server)
                    if not flag:
                        break
                    i = i + 1
                if not flag:
                    break
            if not flag:
                continue

            # Foi possível executar a probabilidade :D
            rack_simulation.potency_watts_usage = rack_simulation.potency_watts_usage + potency_watts_usage
            rack_simulation.electrical_sockets_usage = rack_simulation.electrical_sockets_usage + electrical_sockets_usage
            rack_simulation.ru_usage = rack_simulation.ru_usage + total_us_usage
            rack_simulation.equipaments = equipaments

            list.append([probability, rack, rack_simulation])

        type_result = 2

        if type_result == 1:
            return self.getBestResult(self.getMaxInstalationsResults(list))
        else:
            return self.getBestResult(list)


    # Retorna a(s) probabilidade(s) que teve o maior número de instalações
    def getMaxInstalationsResults(self, results):
        new_list = []
        # Encontra a probabilidade que teve o maior número de instalações
        max_instalations = None
        for result in results:
            instalations = sum(result[0])
            if max_instalations is None:
                max_instalations = instalations
            elif instalations > max_instalations:
                max_instalations = instalations

        for result in results:
            instalations = sum(result[0])
            if instalations == max_instalations:
                new_list.append(result)

        return new_list


    # Retorna a probabilidade que teve o melhor indice (desvio padrão) como resultado
    def getBestResult(self, results):
        results = self.orderResults(results)

        if len(results) == 0:
            return None

        probability = results[0]
        probability.append(results)

        return probability

        '''min_indice = None
        probability = None
        for result in results:
            indice = result[2].getStandardDeviation()
            if min_indice is None:
                min_indice = indice
                probability = result
            elif indice < min_indice:
                min_indice = indice
                probability = result
        if probability is not None:
            probability.append(self.orderResults(results))
        return probability'''


    # Indica se a porta é do mesmo tipo
    def equalsTypePort(self, type_port, type_port_server_simulation):
        if type_port == "RJ-45" and type_port_server_simulation == 2:
            return True
        if type_port != "RJ-45" and type_port_server_simulation == 1:
            return True
        return False


    # Retorna todas as portas do servidor de simulação de um determinado tipo
    def getPortsByType(self, type, ports):
        list = []
        for port in ports:
            if port.type == type:
                list.append(port)
        return list


    # Verifica se existem portas de switchs com as configurações necessárias
    def containsPortsInGroupConnection(self, ports, ports_switch_available_group_connection_converted):
        for equipament_port in ports_switch_available_group_connection_converted[:]:
            if len(ports) == 0:
                break
            for port in ports[:]:
                if equipament_port.velocity == port.velocity and equipament_port.service == port.vlan and self.equalsTypePort(equipament_port.type, port.type):
                    ports.remove(port)
                    ports_switch_available_group_connection_converted.remove(equipament_port)
                    break
        return len(ports) == 0


    # Defini as portas para indisponível (usado). Retorna verdadeiro se foi possível instalar o servidor com as portas disponíveis
    def setPortsUse(self, rack, server):

        ports_switch_available_zone_converted = rack.ports_switch_available_zone_converted

        ports_server_simulation = deepcopy(server.ports)

        # Verifica se existem portas de switch com as configurações
        for port_switch in ports_switch_available_zone_converted[:]:
            if len(ports_server_simulation) == 0:
                break
            for port_server_simulation in ports_server_simulation[:]:
                if port_switch.velocity == port_server_simulation.velocity and port_switch.service == port_server_simulation.vlan and self.equalsTypePort(port_switch.type, port_server_simulation.type):
                    ports_server_simulation.remove(port_server_simulation)
                    ports_switch_available_zone_converted.remove(port_switch)
                    break
        if len(ports_server_simulation) == 0:
            rack.qtd_ports_switch_available = rack.qtd_ports_switch_available - len(server.ports)
            rack.qtd_ports_switch_used = rack.qtd_ports_switch_used + len(server.ports)
            return True

        if len(ports_server_simulation) < len(server.ports):
            qtd_ports_switch_used = len(server.ports) - len(ports_server_simulation)
            rack.qtd_ports_switch_available = rack.qtd_ports_switch_available - qtd_ports_switch_used
            rack.qtd_ports_switch_used = rack.qtd_ports_switch_used + qtd_ports_switch_used

        ports_copper_server = self.getPortsByType(2, ports_server_simulation)
        ports_fiber_server = self.getPortsByType(1, ports_server_simulation)

        qtd_ports_copper_server = len(ports_copper_server)
        qtd_ports_fiber_server = len(ports_fiber_server)

        # Se não tiver portas de cobre ou portas de fibra suficientes para a instalação do servidor
        if qtd_ports_copper_server > rack.qtd_ports_copper_available or qtd_ports_fiber_server > rack.qtd_ports_fiber_available:
            return False

        ports_switch_available_group_connection_converted = rack.ports_switch_available_group_connection_converted
        if len(ports_switch_available_group_connection_converted) == 0:
            return False

        if qtd_ports_copper_server > 0:
            copper = self.containsPortsInGroupConnection(ports_copper_server, ports_switch_available_group_connection_converted)
            if copper:
                rack.qtd_ports_copper_available = rack.qtd_ports_copper_available - qtd_ports_copper_server
                rack.qtd_ports_copper_used = rack.qtd_ports_copper_used + qtd_ports_copper_server
            else:
                return False

        if qtd_ports_fiber_server > 0:
            fiber = self.containsPortsInGroupConnection(ports_fiber_server, ports_switch_available_group_connection_converted)
            if fiber:
                rack.qtd_ports_fiber_available = rack.qtd_ports_fiber_available - qtd_ports_fiber_server
                rack.qtd_ports_fiber_used = rack.qtd_ports_fiber_used + qtd_ports_fiber_server
            else:
                return False

        return True


    # Retorna a tabela que representa o rack, onde cada indice represnta o espaço, e cada espaço tem o valor True (em uso) e False (disponível)
    def loadTableUs(self, total_ru, equipaments):
        table = []
        u_busy = []

        for equipament in equipaments:
            for i in range(equipament.ru_top, (equipament.ru_buttom + 1)):
                u_busy.append(i)

        for i in range(1, (total_ru + 1)):
            flag = False
            for u in u_busy:
                if u == i:
                    flag = True
                    break
            table.append(flag)

        return table


    # Tenta adicionar o servidor
    def addServerUs(self, table_us, equipaments, server):
        for index, u in enumerate(table_us):
            # Se o espaço estiver disponível
            if not u:
                flag = False

                if (index + server.us) > len(table_us):
                    return flag

                for i in range(index, (index + server.us)):
                    if table_us[i]:
                        flag = True
                        break

                # Instala servidor
                if not flag:
                    for i in range(index, (index + server.us)):
                        table_us[i] = True
                    equipament = Equipament()
                    equipament.name = server.name
                    equipament.potency_watts_usage = server.getPotencyWatts()
                    equipament.electrical_sockets_usage = server.electrical_sockets
                    equipament.ru_top = index + 1
                    equipament.ru_size = server.us
                    equipament.ru_buttom = (equipament.ru_top + equipament.ru_size) - 1
                    equipaments.append(equipament)
                    return True

        return False


    # Retorna a matriz de recursos do rack
    def getMatrizResourcesAvailableFromRack(self, rack):
        u = rack.total_ru - rack.ru_usage
        tom = rack.electrical_sockets_limit - rack.electrical_sockets_usage
        pot = rack.potency_watts_limit - rack.potency_watts_usage

        if tom < 0:
            tom = 0
        if pot < 0:
            pot = 0

        co = rack.qtd_ports_copper_available + rack.qtd_ports_switch_copper_available
        fi = rack.qtd_ports_fiber_available + rack.qtd_ports_switch_fiber_available

        return [int(u), int(tom), int(pot), int(co), int(fi)]


    # Retorna a quantidade máxima do servidor para cada recurso
    def getMatrizSimulationFromServer(self, server, matriz_resources_available_rack):
        u = server.us
        tom = server.electrical_sockets
        pot = server.getPotencyWatts()
        co = server.ports_copper
        fi = server.ports_fiber

        return [int(self.divide(matriz_resources_available_rack[0],u)), int(self.divide(matriz_resources_available_rack[1],tom)), \
                int(self.divide(matriz_resources_available_rack[2],pot)), int(self.divide(matriz_resources_available_rack[3],co)), \
                int(self.divide(matriz_resources_available_rack[4],fi))]


    # Retorna a divisão de num1 por num2
    def divide(self, num1, num2):
        if num2 == 0:
            return num1
        return num1 / num2

    # Cálcula todas as probabilidades possíveis de instalação
    def calculateProbability(self, matriz, ind, probabilities):
        if ind == 0:
            probabilities.append([])

        if ind >= len(matriz):
            return

        for i in range(matriz[ind], -1, -1):
            probability = deepcopy(probabilities[len(probabilities)-1])
            probabilities[len(probabilities)-1].append(i)
            self.calculateProbability(matriz, ind+1, probabilities)
            if i > 0:
                probabilities.append(probability)

        if ind == 0:
            return probabilities

        return

    # Elimina as probabilidades que não devem ser testadas
    def clearInvalidProbabilities(self, max, probabilities):
        for index, probability in enumerate(probabilities[:]):
            result = sum(probability)
            if result > max:
                probabilities.remove(probability)
                continue
            if result == 0:
                probabilities.remove(probability)

        return probabilities
