# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db

class BuildingDetails(db.Model):
    __tablename__ = "building_details"

    id_user = db.Column("id_user", db.Integer, primary_key=True)
    id_building = db.Column("id_building", db.Integer, primary_key=True)
    name_building = db.Column("name_building", db.String)
    qtd_switchs = db.Column("qtd_switchs", db.Integer)
    qtd_pathpanels = db.Column("qtd_pathpanels", db.Integer)
    qtd_servers = db.Column("qtd_servers", db.Integer)
    qtd_blades = db.Column("qtd_blades", db.Integer)
    qtd_servers_blades = db.Column("qtd_servers_blades", db.Integer)
    qtd_racks = db.Column("qtd_racks", db.Integer)    
    qtd_ports_copper_available = db.Column("qtd_ports_copper_available", db.Integer)
    qtd_ports_copper_used = db.Column("qtd_ports_copper_used", db.Integer)
    qtd_ports_fiber_available = db.Column("qtd_ports_fiber_available", db.Integer)
    qtd_ports_fiber_used = db.Column("qtd_ports_fiber_used", db.Integer)
    qtd_ports_switch_available = db.Column("qtd_ports_switch_available", db.Integer)
    qtd_ports_switch_used = db.Column("qtd_ports_switch_used", db.Integer)

    def __init__(self):
        pass
