from TsinovaApp import db

class PortsDetails(db.Model):
    __tablename__ = "ports_details"
    is_view = True


    name_room = db.Column("name_room", db.String)
    id_room = db.Column("id_room", db.Integer, primary_key=True)
    name_rack = db.Column("name_rack", db.String)
    id_rack = db.Column("id_rack", db.Integer, primary_key=True)
    zone = db.Column("zone", db.Integer)
    name_equipament = db.Column("name_equipament", db.String)
    id_equipament = db.Column("id_equipament", db.Integer, primary_key=True)
    resource_type_name = db.Column("resource_type_name", db.String)
    name_port = db.Column("name_port", db.String)
    id_equipament_port = db.Column("id_equipament_port", db.Integer, primary_key=True)
    position = db.Column("position", db.Integer)
    type = db.Column("type", db.String)
    velocity = db.Column("velocity", db.String, nullable=True)
    service = db.Column("service", db.String, nullable=True)
    status = db.Column("status", db.Integer)

    def __init__(self):
        pass
