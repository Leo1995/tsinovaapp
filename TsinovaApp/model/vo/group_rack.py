from TsinovaApp import db
import room
import rack

class GroupRack(db.Model):
    __tablename__ = "group_rack"

    id_group_rack = db.Column("id_group_rack", db.Integer, primary_key=True, autoincrement=True)
    id_room = db.Column("id_room", db.Integer, db.ForeignKey("room.id_room"))
    name = db.Column("name", db.String(150), nullable=False)
    description = db.Column("description", db.String(250), nullable=False)
    id_group_rack_by_api = db.Column("id_group_rack_by_api", db.Integer, nullable=True)

    room = db.relationship("Room", uselist=False, backref="group_rack")
    racks = db.relationship("Rack", cascade="save-update, merge, delete")

    def __init__(self, id_group_rack=None, id_room=None, name="", description="", id_group_rack_by_api=None):
        self.id_group_rack = id_group_rack
        self.id_room = id_room
        self.name = name
        self.description = description
        self.id_group_rack_by_api = id_group_rack_by_api

    def __repr__(self):
        return '<GroupRack %r>' % self.name
