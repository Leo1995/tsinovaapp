from TsinovaApp import db
import user
import server_user
import company
import update_server_log

class Server(db.Model):
    __tablename__ = "server"

    id_server = db.Column("id_server", db.Integer, primary_key=True, autoincrement=True)
    name = db.Column("name", db.String(250), nullable=False)
    url = db.Column("url", db.String(250), nullable=False)
    last_update_date = db.Column("last_update_date", db.DateTime, nullable=True)
    ip_database = db.Column("ip_database", db.String(100), nullable=True)
    user_database = db.Column("user_database", db.String(100), nullable=True)
    password_database = db.Column("password_database", db.String(100), nullable=True)
    api_imvision = db.Column("api_imvision", db.String(200), nullable=True)
    user_api = db.Column("user_api", db.String(45), nullable=True)
    password_api = db.Column("password_api", db.String(45), nullable=True)
    off_air = db.Column("off_air", db.Boolean, nullable=False)

    users = db.relationship("User", secondary="server_user", backref='server')
    companies = db.relationship("Company", cascade="save-update, merge, delete")
    server_users = db.relationship("ServerUser", cascade="save-update, merge, delete")
    update_server_logs = db.relationship("UpdateServerLog", cascade="save-update, merge, delete")

    def __init__(self, id_server=None, name=None, url="", last_update_date=None):
        self.id_server = id_server
        self.name = name
        self.url = url
        self.last_update_date = last_update_date

    def __repr__(self):
        return '<Server %r>' % self.name
