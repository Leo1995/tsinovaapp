from TsinovaApp import db
import server_simulation

class PortsServerSimulation(db.Model):
    __tablename__ = "ports_server_simulation"

    id_ports_server_simulation = db.Column("id_ports_server_simulation", db.Integer, primary_key=True, autoincrement=True)
    type = db.Column("type", db.Integer, nullable=True)
    velocity = db.Column("velocity", db.String(45), nullable=True)
    vlan = db.Column("vlan", db.String(45), nullable=True)
    id_server_simulation = db.Column("id_server_simulation", db.Integer, db.ForeignKey("server_simulation.id_server_simulation"))

    server_simulation = db.relationship("ServerSimulation", uselist=False, backref="ports_server_simulation")

    def __init__(self):
        pass

    def __repr__(self):
        return '<PortsServerSimulation %r>' % self.id_ports_server_simulation
