from TsinovaApp import db
import company
import floor

class Building(db.Model):
    __tablename__ = "building"

    id_building = db.Column("id_building", db.Integer, primary_key=True, autoincrement=True)
    id_company = db.Column("id_company", db.Integer, db.ForeignKey("company.id_company"))
    name = db.Column("name", db.String(150), nullable=False)
    description = db.Column("description", db.String(250), nullable=False)
    id_building_by_api = db.Column("id_building_by_api", db.Integer, nullable=True)

    company = db.relationship("Company", uselist=False, backref="building")
    floors = db.relationship("Floor", cascade="save-update, merge, delete")

    def __init__(self, id_company=None, name="", description="", id_building_by_api=None):
        self.id_company = id_company
        self.name = name
        self.description = description
        self.id_building_by_api = id_building_by_api

    def __repr__(self):
        return '<Building %r>' % self.name
