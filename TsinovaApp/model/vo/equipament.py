# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
import rack
import equipament_ports
from slot import Slot

class Equipament(db.Model):
    __tablename__ = "equipament"

    id_equipament = db.Column("id_equipament", db.Integer, primary_key=True, autoincrement=True)
    id_rack = db.Column("id_rack", db.Integer, db.ForeignKey("rack.id_rack"))
    name = db.Column("name", db.String(150), nullable=False)
    description = db.Column("description", db.String(250), nullable=False)
    potency_watts_usage = db.Column("potency_watts_usage", db.Float, nullable=True,  default=0)
    electrical_sockets_usage = db.Column("electrical_sockets_usage", db.Integer, nullable=True,  default=0)
    ru_top = db.Column("ru_top", db.Integer, nullable=True)
    ru_buttom = db.Column("ru_buttom", db.Integer, nullable=True)
    ru_size = db.Column("ru_size", db.Integer, nullable=True)
    resource_type_name = db.Column("resource_type_name", db.String(100))
    created_date = db.Column("created_date", db.DateTime, nullable=True)
    id_equipament_by_api = db.Column("id_equipament_by_api", db.Integer, nullable=True)
    id_equipament_parent = db.Column("id_equipament_parent", db.Integer, db.ForeignKey("equipament.id_equipament"), nullable=True)


    rack = db.relationship("Rack", uselist=False, backref="equipament")
    equipament_ports = db.relationship("EquipamentPorts",
                                       cascade="save-update, merge, delete",
                                       order_by="asc(EquipamentPorts.id_slot_by_api), asc(EquipamentPorts.position)")

    equipaments = db.relationship("Equipament", cascade="save-update, merge, delete", primaryjoin="and_(Equipament.id_equipament==Equipament.id_equipament_parent)");

    def __init__(self, id_equipament=None, id_rack=None,
                 name="", description="", ru_top=None, ru_buttom=None,
                 ru_size=None, resource_type_name=None, id_equipament_by_api=None,
                 potency_watts_usage=0, electrical_sockets_usage=0):
        self.id_equipament = id_equipament
        self.id_rack = id_rack
        self.name = name
        self.description = description
        self.ru_top = ru_top
        self.ru_buttom = ru_buttom
        self.ru_size = ru_size
        self.resource_type_name = resource_type_name
        self.id_equipament_by_api = id_equipament_by_api
        self.potency_watts_usage = potency_watts_usage
        self.electrical_sockets_usage = electrical_sockets_usage


    # Retorna o serviço
    def getService(self):
        service = None
        for index, port in enumerate(self.equipament_ports):
            if index == 0:
                service = port.service
                continue
            if port.service != service:
                service = "Misto"
                break
        return service


    # Retorna um slot através de um ID
    def getSlotByID(self, id_slot_by_api):
        slots = self.getSlots()
        for slot in slots:
            if slot.id_slot == id_slot_by_api:
                return slot
        return None


    # Retorna todos os slots
    def getSlots(self):
        list = []
        for port in self.equipament_ports:
            if port.id_slot_by_api is None:
                continue
            slot_old = None
            for obj in list:
                if obj.id_slot == port.id_slot_by_api:
                    slot_old = obj
                    break
            if slot_old is not None:
                slot_old.ports.append(port)
            if slot_old is None:
                list.append(Slot(port.id_slot_by_api, port.slot_name, port.slot_description, port))

        return list


    # Retorna todas as portas não conectadas a um slot
    def getPorts(self):
        list = []
        for port in self.equipament_ports:
            if port.id_slot_by_api is None:
                list.append(port)
        return list


    def __repr__(self):
        return '<Equipament %r>' % self.name
