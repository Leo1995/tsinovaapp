from TsinovaApp import db
import rack
import group_connection

class GroupRackConnection(db.Model):
    __tablename__ = "group_rack_connection"
    __table_args__ = {'extend_existing': True}

    id_rack = db.Column("id_rack", db.Integer, db.ForeignKey("rack.id_rack"), primary_key=True)
    id_group_connection = db.Column("id_group_connection", db.Integer, db.ForeignKey("group_connection.id_group_connection"), primary_key=True)

    rack = db.relationship("Rack", uselist=False, backref="group_rack_connection")
    group_connection = db.relationship("GroupConnection", uselist=False, backref="group_rack_connection")

    def __init__(self):
        pass

    def __repr__(self):
        return '<GroupRackConnection %r>' % self.id_rack
