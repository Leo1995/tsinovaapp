# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
import equipament

class EquipamentPorts(db.Model):
    __tablename__ = "equipament_ports"

    id_equipament_port = db.Column("id_equipament_port", db.Integer, primary_key=True, autoincrement=True)
    id_equipament = db.Column("id_equipament", db.Integer, db.ForeignKey("equipament.id_equipament"))
    position = db.Column("position", db.Integer, nullable=True)
    name = db.Column("name", db.String(150), nullable=True)
    description = db.Column("description", db.String(250), nullable=True)
    status = db.Column("status", db.Integer, nullable=True)
    service = db.Column("service", db.String(100), nullable=True)
    velocity = db.Column("velocity", db.String(100), nullable=True)
    type = db.Column("type", db.String(100), nullable=True)
    id_slot_by_api = db.Column("id_slot_by_api", db.Integer, nullable=True)
    slot_name = db.Column("slot_name", db.String(100), nullable=True)
    slot_description = db.Column("slot_description", db.String(100), nullable=True)
    id_port_by_api = db.Column("id_port_by_api", db.Integer, nullable=True)

    def __init__(self):
        pass

    def __repr__(self):
        return '<EquipamentPorts %r>' % self.name

    def getStatusToString(self):
        if self.status is None:
            return ""
        if self.status == 1:
            return "Disponível"
        if self.status == 2:
            return "Em uso"
        if self.status == 3:
            return "Indisponível (sem utilidade)"
        return "Desconhecido"
