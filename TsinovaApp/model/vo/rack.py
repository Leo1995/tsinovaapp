# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
import room
import group_rack
import equipament
import group_connection
import group_rack_connection

class Rack(db.Model):
    __tablename__ = "rack"

    id_rack = db.Column("id_rack", db.Integer, primary_key=True, autoincrement=True)
    id_room = db.Column("id_room", db.Integer, db.ForeignKey("room.id_room"))
    id_group_rack = db.Column("id_group_rack", db.Integer, db.ForeignKey("group_rack.id_group_rack"), nullable=True)
    name = db.Column("name", db.String(150), nullable=False)
    description = db.Column("description", db.String(250), nullable=False)
    zone = db.Column("zone", db.Integer, nullable=False)
    position_zone = db.Column("position_zone", db.Integer, nullable=False)
    total_ru = db.Column("total_ru", db.Integer, nullable=False)
    potency_watts_limit = db.Column("potency_watts_limit", db.Float, nullable=True)
    electrical_sockets_limit = db.Column("electrical_sockets_limit", db.Integer, nullable=True)
    id_rack_by_api = db.Column("id_rack_by_api", db.Integer, nullable=True)
    participate_calculation = db.Column("participate_calculation", db.Boolean, nullable=False, default=1)
    created_date = db.Column("created_date", db.DateTime, nullable=True)

    room = db.relationship("Room", uselist=False, backref="rack")
    group_rack = db.relationship("GroupRack", uselist=False, backref="rack")
    equipaments = db.relationship("Equipament", cascade="save-update, merge, delete", order_by="asc(Equipament.ru_top)")

    group_connections = db.relationship("GroupConnection", secondary="group_rack_connection", backref='rack')
    group_rack_connections = db.relationship("GroupRackConnection", cascade="save-update, merge, delete")


    def __init__(self):
        pass

    def __repr__(self):
        return '<Rack %r>' % self.name

    # Retorna a posição para instalação do servidor
    def getPositionServerForInstall(self, us):
        table_us = self.loadTableUs(self.total_ru, self.equipaments)
        for index, u in enumerate(table_us):
            if not u:
                flag = False
                if (index + us) > len(table_us):
                    return flag
                for i in range(index, (index + us)):
                    if table_us[i]:
                        flag = True
                        break
                # Posição para instalação do servidor
                if not flag:
                    return index + 1
        return False

    # Retorna a tabela que representa o rack, onde cada indice represnta o espaço, e cada espaço tem o valor True (em uso) e False (disponível)
    def loadTableUs(self, total_ru, equipaments):
        table = []
        u_busy = []
        for equipament in equipaments:
            for i in range(equipament.ru_top, (equipament.ru_buttom + 1)):
                u_busy.append(i)
        for i in range(1, (total_ru + 1)):
            flag = False
            for u in u_busy:
                if u == i:
                    flag = True
                    break
            table.append(flag)
        return table


    # Retorna o local de instalação do rack
    def getLocalInstallation(self):
        path = ""
        path = self.room.floor.building.company.name + " / "
        path += self.room.floor.building.name + " / "
        path += self.room.floor.name + " / "
        path += self.room.name + " / "
        if self.id_group_rack is not None:
            path += self.group_rack.name + " / "
        path += "Fileira: " + str(self.zone) + ", Posição: " + str(self.position_zone)
        return path
