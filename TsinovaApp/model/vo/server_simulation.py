# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
import ports_server_simulation
from sqlalchemy import inspect
from electrical_parameters import ElectricalParameters


class ServerSimulation(db.Model):
    __tablename__ = "server_simulation"

    id_server_simulation = db.Column("id_server_simulation", db.Integer, primary_key=True, autoincrement=True)
    name = db.Column("name", db.String(100), nullable=False)
    us = db.Column("us", db.Integer, nullable=True)
    ports_copper = db.Column("ports_copper", db.Integer, nullable=True)
    ports_fiber = db.Column("ports_fiber", db.Integer, nullable=True)
    potency_watts = db.Column("potency_watts", db.Float, nullable=True)
    electrical_sockets = db.Column("electrical_sockets", db.Integer, nullable=True)

    ports = db.relationship("PortsServerSimulation", cascade="save-update, merge, delete")

    def __init__(self):
        pass

    def __repr__(self):
        return '<ServerSimulation %r>' % self.name

    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }

    def getPotencyWatts(self):
        electrical_parameters = ElectricalParameters.query.first()
        return self.potency_watts * electrical_parameters.potency_indice

    def getPortsByType(self, type):
        ports = []
        for port in self.ports:
            if port.type == type:
                ports.append(port)
        return ports


    def toString(self):
        return self.name + \
               " | Us: " + str(self.us) + \
               " | P.C: " + str(self.ports_copper) + \
               " | P.F: " + str(self.ports_fiber) + \
               " | Potência: " + str(self.potency_watts) + \
               " | Tomadas: " + str(self.electrical_sockets)
