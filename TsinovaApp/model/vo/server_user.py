from TsinovaApp import db
import server
import user

class ServerUser(db.Model):
    __tablename__ = "server_user"
    __table_args__ = {'extend_existing': True}

    id_user = db.Column("id_user", db.Integer, db.ForeignKey("user.id_user"), primary_key=True)
    id_server = db.Column("id_server", db.Integer, db.ForeignKey("server.id_server"), primary_key=True)

    user = db.relationship("User", uselist=False, backref="server_user")
    server = db.relationship("Server", uselist=False, backref="server_user")

    def __init__(self, id_user=None, id_server=None):
        self.id_user = id_user
        self.id_server = id_server

    def __repr__(self):
        return '<ServerUser %r>' % self.id_server
