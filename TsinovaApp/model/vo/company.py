from TsinovaApp import db
import server
import building

class Company(db.Model):
    __tablename__ = "company"

    id_company = db.Column("id_company", db.Integer, primary_key=True, autoincrement=True)
    id_server = db.Column("id_server", db.Integer, db.ForeignKey("server.id_server"))
    name = db.Column("name", db.String(150), nullable=False)
    description = db.Column("description", db.String(250), nullable=False)
    id_company_by_api = db.Column("id_company_by_api", db.Integer, nullable=True)

    server = db.relationship("Server", uselist=False, backref="company")
    buildings = db.relationship("Building", cascade="save-update, merge, delete")

    def __init__(self, name="", description="", id_company_by_api=None):
        self.name = name
        self.description = description
        self.id_company_by_api = id_company_by_api

    def __repr__(self):
        return '<Company %r>' % self.name
