# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
import floor
import rack
import group_rack
from util.config import *
from sqlalchemy import text
from model.vo.ports_details import PortsDetails

class Room(db.Model):
    __tablename__ = "room"

    id_room = db.Column("id_room", db.Integer, primary_key=True, autoincrement=True)
    id_floor = db.Column("id_floor", db.Integer, db.ForeignKey("floor.id_floor"))
    name = db.Column("name", db.String(150), nullable=False)
    description = db.Column("description", db.String(250), nullable=False)
    id_room_by_api = db.Column("id_room_by_api", db.Integer, nullable=True)

    floor = db.relationship("Floor", uselist=False, backref="room")
    racks = db.relationship("Rack", cascade="save-update, merge, delete")
    group_racks = db.relationship("GroupRack", cascade="save-update, merge, delete")

    def __init__(self, id_room=None, id_floor=None, name="", description="", id_room_by_api=None):
        self.id_room = id_room
        self.id_floor = id_floor
        self.name = name
        self.description = description
        self.id_room_by_api = id_room_by_api

    def __repr__(self):
        return '<Room %r>' % self.name
