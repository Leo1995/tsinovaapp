import work_order_task
from datetime import datetime, timedelta

class WorkOrder():

    def __init__(self):
        self.name = ""
        self.orderState = None
        self.orderStateName = None
        self.orderTypeName = None
        self.dateStart = None
        self.dateEnd = None
        self.technician = ""
        self.description = ""
        self.tasks = []

    def getDateByString(self, date):
         return date.strftime("%d/%m/%Y %H:%M:%S")
