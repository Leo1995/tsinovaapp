# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
import equipament_ports

class Slot:

    def __init__(self, id_slot=None, name=None, description=None, port=None):
        self.id_slot = id_slot
        self.name = name
        self.description = description
        self.ports = []
        self.ports.append(port)

    # Retorna o serviço
    def getService(self):
        service = None
        for index, port in enumerate(self.ports):
            if index == 0:
                service = port.service
                continue
            if port.service != service:
                service = "Misto"
                break
        return service
