# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from math import *
import statistics
from decimal import Decimal
from util.util import *
import json
from copy import deepcopy
from limiting import Limiting
from equipament import Equipament
from equipament_ports import EquipamentPorts
import json
from flask import Markup

class RacksDetails(db.Model):
    __tablename__ = "racks_details"

    id_user = db.Column("id_user", db.Integer, primary_key=True)
    id_server = db.Column("id_server", db.Integer, primary_key=True)
    id_company = db.Column("id_company", db.Integer, primary_key=True)
    id_building = db.Column("id_building", db.Integer, primary_key=True)
    name_building = db.Column("name_building", db.String)
    id_floor = db.Column("id_floor", db.Integer, primary_key=True)
    id_room = db.Column("id_room", db.Integer, primary_key=True)
    id_rack = db.Column("id_rack", db.Integer, primary_key=True)
    name_rack = db.Column("name_rack", db.String)
    local_installation = db.Column("local_installation", db.String)
    zone = db.Column("zone", db.Integer)
    position_zone = db.Column("position_zone", db.Integer)
    total_ru = db.Column("total_ru", db.Integer)
    participate_calculation = db.Column("participate_calculation", db.Boolean)
    potency_watts_limit = db.Column("potency_watts_limit", db.Float)
    electrical_sockets_limit = db.Column("electrical_sockets_limit", db.Integer)
    potency_watts_usage = db.Column("potency_watts_usage", db.Float)
    electrical_sockets_usage = db.Column("electrical_sockets_usage", db.Integer)
    ru_usage = db.Column("ru_usage", db.Integer)
    qtd_ports_copper_available = db.Column("qtd_ports_copper_available", db.Integer)
    qtd_ports_copper_used = db.Column("qtd_ports_copper_used", db.Integer)
    qtd_ports_fiber_available = db.Column("qtd_ports_fiber_available", db.Integer)
    qtd_ports_fiber_used = db.Column("qtd_ports_fiber_used", db.Integer)
    qtd_ports_switch_available = db.Column("qtd_ports_switch_available", db.Integer)
    qtd_ports_switch_copper_available = db.Column("qtd_ports_switch_copper_available", db.Integer)
    qtd_ports_switch_fiber_available = db.Column("qtd_ports_switch_fiber_available", db.Integer)
    qtd_ports_switch_used = db.Column("qtd_ports_switch_used", db.Integer)
    ports_switch_available_by_zone = db.Column("ports_switch_available", db.Text)
    ports_switch_available_connection = db.Column("ports_switch_available_connection", db.Text)
    limiting = None

    def __init__(self):
        self.equipaments = None
        self.ports_switch_available_zone_converted = None
        self.ports_switch_available_group_connection_converted = None
        pass

    def loadEquipaments(self):
        self.equipaments = Equipament.query.filter(Equipament.id_rack == self.id_rack).\
                                            filter(Equipament.id_equipament_parent == None).all()

    def loadPortsSwitchAvailableZoneByJson(self):
        list = []
        for equipament_port in json.loads(self.ports_switch_available_by_zone):
            port = EquipamentPorts()
            port.velocity = equipament_port['velocity']
            port.service = equipament_port['service']
            port.type = equipament_port['type']
            port.status = equipament_port['status']
            list.append(port)
        self.ports_switch_available_zone_converted = list


    def loadPortsSwitchAvailableGroupConnectionByJson(self):
        list = []
        for ports_switch_by_rack in json.loads(self.ports_switch_available_connection):
            for port in json.loads(ports_switch_by_rack['ports']):
                equipament_port = EquipamentPorts()
                equipament_port.service = port['service']
                equipament_port.velocity = port['velocity']
                equipament_port.type = port['type']
                equipament_port.status = port['status']
                list.append(equipament_port)
        self.ports_switch_available_group_connection_converted = list


    def getUsUsagePorcentage(self):
        if self.total_ru == 0:
            return 0
        return formatValue((self.ru_usage * 100) / self.total_ru, 1)

    def getElectricalSocketsUsagePorcentage(self):
        if self.electrical_sockets_limit == 0:
            return 0
        return formatValue((self.electrical_sockets_usage * 100) / self.electrical_sockets_limit, 1)

    def getPotencyUsagePorcentage(self):
        if self.potency_watts_limit == 0:
            return 0
        return formatValue((self.potency_watts_usage * 100) / self.potency_watts_limit, 1)

    def getQtdPortsSwitchUsagePorcentage(self):
        total = self.qtd_ports_switch_available + self.qtd_ports_switch_used
        if total == 0:
            return 0
        return formatValue((self.qtd_ports_switch_used * 100) / total, 1)

    def getQtdPortsCopperUsagePorcentage(self):
        total = self.qtd_ports_copper_available + self.qtd_ports_copper_used
        if total == 0:
            return 0
        return formatValue((self.qtd_ports_copper_used * 100) / total, 1)

    def getQtdPortsFiberUsagePorcentage(self):
        total = self.qtd_ports_fiber_available + self.qtd_ports_fiber_used
        if total == 0:
            return 0
        return formatValue((self.qtd_ports_fiber_used * 100) / total, 1)

    def getStandardDeviation(self):
        data = []
        data.append(float(self.getUsUsagePorcentage()))
        if self.potency_watts_limit > 0:
            data.append(float(self.getPotencyUsagePorcentage()))
        if self.electrical_sockets_limit > 0:
            data.append(float(self.getElectricalSocketsUsagePorcentage()))
        if (self.qtd_ports_copper_available + self.qtd_ports_copper_used) > 0:
            data.append(float(self.getQtdPortsCopperUsagePorcentage()))
        if (self.qtd_ports_fiber_available + self.qtd_ports_fiber_used) > 0:
            data.append(float(self.getQtdPortsFiberUsagePorcentage()))
        if (self.qtd_ports_switch_available + self.qtd_ports_switch_used) > 0:
            data.append(float(self.getQtdPortsSwitchUsagePorcentage()))
        return formatValue(statistics.pstdev(data)/100.0, 6)

    def toString(self):
        return ("Us: " + str(self.getUsUsagePorcentage()) + \
               "Potencia: " + str(self.getPotencyUsagePorcentage()) + \
               "Tomadas: " + str(self.getElectricalSocketsUsagePorcentage()) + \
               "Cobre: " + str(self.getQtdPortsCopperUsagePorcentage()) + \
               "Fibra: " + str(self.getQtdPortsFiberUsagePorcentage()) + \
               "Switch: " + str(self.getQtdPortsSwitchUsagePorcentage()))

    # -------------------------- INSTALAR SERVIDOR ----------------------------- #

    # Adiciona us em uso
    def setUsUse(self, us):
        us_available = self.total_ru - self.ru_usage
        if us_available < us:
            return False
        self.ru_usage = self.ru_usage + us
        return True

    # Adiciona Potencia em uso
    def setPotencyUse(self, potency):
        potency_available = self.potency_watts_limit - self.potency_watts_usage
        if potency_available < potency:
            return False
        self.potency_watts_usage = self.potency_watts_usage + potency
        return True

    # Adiciona tomadas em uso
    def setElectricalUse(self, electrical):
        electrical_sockets_available = self.electrical_sockets_limit - self.electrical_sockets_usage
        if electrical_sockets_available < electrical:
            return False
        self.electrical_sockets_usage = self.electrical_sockets_usage + electrical
        return True

    # Retorna todas as portas do servidor de simulação de um determinado tipo
    def getPortsByType(self, type, ports):
        list = []
        for port in ports:
            if port.type == type:
                list.append(port)
        return list

    # Retorna as portas de switch disponíveis em racks de outros grupos
    def getPortsSwitchAvailableByGroupConnection(self):
        list = []
        for ports_switch_by_rack in json.loads(self.ports_switch_available_connection):
            for port in json.loads(ports_switch_by_rack['ports']):
                equipament_port = EquipamentPorts()
                equipament_port.service = port['service']
                equipament_port.velocity = port['velocity']
                list.append(equipament_port)
        return list

    # Verifica se existem portas de switchs com as configurações necessárias
    def containsPortsInGroupConnection(self, ports, ports_switch_available_group_connection):
        for equipament_port in ports_switch_available_group_connection[:]:
            if len(ports) == 0:
                break
            for port in ports[:]:
                if equipament_port.velocity == port.velocity and equipament_port.service == port.vlan:
                    ports.remove(port)
                    ports_switch_available_group_connection.remove(equipament_port)
                    break
        return len(ports) == 0

    # Adicionar portas em uso
    def setPortsUse(self, server_simulation):

        ports = deepcopy(server_simulation.ports)

        # Verifica se existem portas de switch com as configurações
        ports_switch = json.loads(self.ports_switch_available_by_zone)
        for equipament_port in ports_switch:
            if len(ports) == 0:
                break
            for port in ports[:]:
                if equipament_port['velocity'] == port.velocity and equipament_port['service'] == port.vlan:
                    ports.remove(port)
                    break

        if len(ports) == 0:
            self.qtd_ports_switch_available = self.qtd_ports_switch_available - len(server_simulation.ports)
            self.qtd_ports_switch_used = self.qtd_ports_switch_used + len(server_simulation.ports)
            return [True, True]

        if len(ports) < len(server_simulation.ports):
            qtd_ports_switch_used = len(server_simulation.ports) - len(ports)
            self.qtd_ports_switch_available = self.qtd_ports_switch_available - qtd_ports_switch_used
            self.qtd_ports_switch_used = self.qtd_ports_switch_used + qtd_ports_switch_used


        qtd_ports_copper_server = len(self.getPortsByType(2, ports))
        qtd_ports_fiber_server = len(self.getPortsByType(1, ports))

        copper = False
        fiber = False

        if qtd_ports_copper_server <= self.qtd_ports_copper_available:
            copper = True

        if qtd_ports_fiber_server <= self.qtd_ports_fiber_available:
            fiber = True

        # Se não tiver portas de cobre nem portas de fibra
        if not copper and not fiber:
            return [False, False]

        # Pega todas as portas de todos os switchs do grupo
        ports_switch_available_group_connection = self.getPortsSwitchAvailableByGroupConnection()
        if len(ports_switch_available_group_connection) == 0:
            return [False, False]

        # Se tiverem portas de cobre suficiente, verifica se existem portas de switchs com as configurações no grupo
        if copper and qtd_ports_copper_server > 0:
            copper = self.containsPortsInGroupConnection(self.getPortsByType(2, ports), ports_switch_available_group_connection)
            if copper:
                self.qtd_ports_copper_available = self.qtd_ports_copper_available - qtd_ports_copper_server
                self.qtd_ports_copper_used = self.qtd_ports_copper_used + qtd_ports_copper_server

        # Se tiverem portas de cobre suficiente, verifica se existem portas de switchs com as configurações no grupo
        if fiber and qtd_ports_fiber_server > 0:
            fiber = self.containsPortsInGroupConnection(self.getPortsByType(1, ports), ports_switch_available_group_connection)
            if fiber:
                self.qtd_ports_fiber_available = self.qtd_ports_fiber_available - qtd_ports_fiber_server
                self.qtd_ports_fiber_used = self.qtd_ports_fiber_used + qtd_ports_fiber_server

        return [copper, fiber]


    # ------------------------------------------------------------------------------------------------------------------ #

    def getEquipamentByPositionInRack(self, equipaments, position):
        for equipament in equipaments:
            if equipament.ru_top == position:
                return equipament
        return None

    def addRowEmpty(self, rows, position):
        rows += "<tr class='row_us_table'>" + \
                "<td class='center_horizontal column_id_us_table'>" + str(position) + "</td>" + \
                "<td class='center_horizontal column_us_table_available'></td></tr>"
        return rows

    def addRowFirstBusy(self, rows, equipament, position):
        if equipament.id_equipament is None:
            rows += "<tr class='row_us_table'>" + \
                    "<td class='center_horizontal column_id_us_table'>"+str(position)+"</td>" + \
                    "<td class='center_horizontal column_us_table_new' rowspan='"+str(equipament.ru_size)+"'>"+equipament.name+"</td>" + \
                    "</tr>"
        else:
            rows += "<tr class='row_us_table'>" + \
                    "<td class='center_horizontal column_id_us_table'>"+str(position)+"</td>" + \
                    "<td class='center_horizontal column_us_table_busy' rowspan='"+str(equipament.ru_size)+"'>"+equipament.name+"</td>" + \
                    "</tr>"
        return rows

    def addRowNextBusy(self, rows, position):
        rows += "<tr class='row_us_table'>" + \
                "<td class='center_horizontal column_id_us_table'>"+str(position)+"</td>" + \
                "</tr>"
        return rows


    def getEstruturaHtml(self):
        rows = ""
        i = 1
        while (i <= self.total_ru):
            equipament = self.getEquipamentByPositionInRack(self.equipaments, i)
            if equipament is None:
                rows = self.addRowEmpty(rows, i)
            else:
                first = False
                for p in range(equipament.ru_top, (equipament.ru_buttom + 1)):
                    if not first:
                        rows = self.addRowFirstBusy(rows, equipament, p)
                        first = True
                    else:
                        rows = self.addRowNextBusy(rows, p)
                    i = p
            i = i + 1
        return Markup(rows)
