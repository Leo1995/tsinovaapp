from TsinovaApp import db
import building
import room

class Floor(db.Model):
    __tablename__ = "floor"

    id_floor = db.Column("id_floor", db.Integer, primary_key=True, autoincrement=True)
    id_building = db.Column("id_building", db.Integer, db.ForeignKey("building.id_building"))
    number = db.Column("number", db.Integer, nullable=False)
    name = db.Column("name", db.String(150), nullable=False)
    description = db.Column("description", db.String(250), nullable=False)
    id_floor_by_api = db.Column("id_floor_by_api", db.Integer, nullable=True)

    building = db.relationship("Building", uselist=False, backref="floor")
    rooms = db.relationship("Room", cascade="save-update, merge, delete")

    def __init__(self, id_floor=None, id_building=None, number=None,
                 name="", description="", id_floor_by_api=None):
        self.id_floor = id_floor
        self.id_building = id_building
        self.number = number
        self.name = name
        self.description = description
        self.id_floor_by_api = id_floor_by_api

    def __repr__(self):
        return '<Floor %r>' % self.name
