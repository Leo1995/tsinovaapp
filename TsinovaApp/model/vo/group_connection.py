# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
import rack
import group_rack_connection

class GroupConnection(db.Model):
    __tablename__ = "group_connection"

    id_group_connection = db.Column("id_group_connection", db.Integer, primary_key=True, autoincrement=True)
    name = db.Column("name", db.String(100), nullable=False)

    racks = db.relationship("Rack", secondary="group_rack_connection", backref='group_connection')
    group_rack_connections = db.relationship("GroupRackConnection", cascade="save-update, merge, delete")

    def __init__(self):
        pass

    def __repr__(self):
        return '<GroupConnection %r>' % self.name

    def getIdsRacksToString(self):
        ids = ""
        for index, rack in enumerate(self.racks):
            if not ids:
                ids = str(rack.id_rack)
            else:
                ids += "," + str(rack.id_rack)
        return ids
