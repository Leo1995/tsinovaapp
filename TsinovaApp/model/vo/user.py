from TsinovaApp import db
import server_user
import server
from flask_login import UserMixin


class User(UserMixin, db.Model):
    __tablename__ = "user"

    id_user = db.Column("id_user", db.Integer, primary_key=True, autoincrement=True)
    name = db.Column("name", db.String(150), nullable=False)
    email = db.Column("email", db.String(150), nullable=False)
    password = db.Column("password", db.String(200), nullable=False)

    servers = db.relationship("Server", secondary="server_user", backref='user')
    server_users = db.relationship("ServerUser", cascade="save-update, merge, delete")

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.email

    def get_id(self):
        return self.id_user

    def getIdsServers(self):
        ids_server = None
        for server in self.servers:
            if ids_server is None:
                ids_server = str(server.id_server)
            else:
                ids_server = ids_server + ";" + str(server.id_server)
        return ids_server
