from TsinovaApp import db
from model.vo.server import Server

class ServerDao(object):

    @staticmethod
    def insertServer(server):
        db.session.add(server)

    @staticmethod
    def getServerById(id):
        return Server.query.filter_by(id_server = id).first()

    @staticmethod
    def removeServer(id_server):
        server = Server.query.filter_by(id_server=id_server).first()
        for server_user in server.server_users:
            db.session.delete(server_user)
            db.session.flush()
        db.session.delete(server)
        db.session.flush()
