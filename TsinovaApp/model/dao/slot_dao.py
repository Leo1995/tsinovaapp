# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.equipament_ports import EquipamentPorts
from model.vo.slot import Slot
from model.vo.server import Server

class SlotDao(object):

    # Atualiza os dados dos slots
    @staticmethod
    def updateByAPI(server, slot_json):
        for company in server.companies:
            for building in company.buildings:
                for floor in building.floors:
                    for room in floor.rooms:
                        for rack in room.racks:
                            for equipament in rack.equipaments:
                                if equipament.id_equipament_by_api != slot_json['id_parent']:
                                    continue
                                slot = equipament.getSlotByID(slot_json['id'])
                                if slot is None:
                                    continue
                                # Atualizar
                                if slot_json['action'] == 3:
                                    service_slot_old = slot.getService()
                                    for equipament_port in slot.ports:
                                        equipament_port.slot_name = slot_json['name']
                                        equipament_port.slot_description = slot_json['description']
                                        if service_slot_old != "Misto":
                                            service = slot_json['service']
                                            if service_slot_old != service:
                                                equipament_port.service = service
                                    db.session.flush()
                                    return
                                if slot_json['action'] == 4:
                                    for equipament_port in slot.ports:
                                        db.session.delete(equipament_port)
                                    db.session.flush()
                                    return
