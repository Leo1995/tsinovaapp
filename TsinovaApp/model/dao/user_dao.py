from TsinovaApp import db
from model.vo.user import User

class UserDao(object):


    @staticmethod
    def insertUser(user):
        db.session.add(user)

    @staticmethod
    def removeUser(id_user):
        user = User.query.filter_by(id_user=id_user).first()
        for server_user in user.server_users:
            db.session.delete(server_user)
            db.session.flush()
        db.session.delete(user)
        db.session.flush()

    @staticmethod
    def insert(user):
        db.session.add(user)
        db.session.commit()

    @staticmethod
    def getUserById(id):
        return User.query.filter_by(id_user = id).first()

    @staticmethod
    def getUserByEmailAndPassword(email, password):
        return User.query.filter_by(email = email, password = password).first()
