# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.floor import Floor
from model.vo.server import Server

class FloorDao(object):

    # Atualiza os dados dos andares
    @staticmethod
    def updateByAPI(server, floor_json):
        for company in server.companies:
            for building in company.buildings:
                if building.id_building_by_api != floor_json['id_parent']:
                    continue
                # Criar um novo andar
                if floor_json['action'] == 2:
                    floor = Floor()
                    floor.name = floor_json['name']
                    floor.description = floor_json['description']
                    floor.id_floor_by_api = floor_json['id']
                    floor.number = 1
                    building.floors.append(floor)
                    db.session.flush()
                    return
                # Atualizar ou remover
                for floor in building.floors:
                    if floor.id_floor_by_api != floor_json['id']:
                        continue
                    # atualizar cadastro
                    if floor_json['action'] == 3:
                        floor.name = floor_json['name']
                        floor.description = floor_json['description']
                        floor.number = 1
                    # Remover cadastro
                    if floor_json['action'] == 4:
                        db.session.delete(floor)
                    db.session.flush()
                    return
