# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.update_server_log import UpdateServerLog

class UpdateServerLogDao(object):

    # Insere um log
    @staticmethod
    def insertLog(update_server_log, transaction=False):
        try:
            db.session.add(update_server_log)
            db.session.flush()
        except:
            raise
        else:
            if not transaction:
                db.session.commit()
