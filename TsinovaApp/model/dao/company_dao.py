# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.company import Company
from model.vo.server import Server
from model.vo.user import User
from model.vo.server_user import ServerUser

class CompanyDao(object):

    # Retorna todas as empresas de um determinado usuário
    @staticmethod
    def getCompaniesByUser(id_user):
        list = []
        result = db.session.query(User, ServerUser, Server, Company).\
        filter(User.id_user == ServerUser.id_user).\
        filter(ServerUser.id_server == Server.id_server).\
        filter(Server.id_server == Company.id_server).\
        filter(User.id_user == id_user).\
        all()
        for obj in result:
            list.append(obj[3])
        return list


    # Atualiza os dados da empresa
    @staticmethod
    def updateByAPI(server, company_json):
        # Criar nova empresa
        if company_json['action'] == 2:
            company = Company()
            company.id_server = server.id_server
            company.name = company_json['name']
            company.description = company_json['description']
            company.id_company_by_api = company_json['id']
            server.companies.append(company)
            db.session.flush()
            return
        # Atualizar ou remover
        for company in server.companies:
            if company.id_company_by_api != company_json['id']:
                continue
            # atualizar cadastro
            if company_json['action'] == 3:
                company.name = company_json['name']
                company.description = company_json['description']
            # Remover cadastro
            if company_json['action'] == 4:
                db.session.delete(company)
            db.session.flush()
            return
