# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.equipament_ports import EquipamentPorts
from model.vo.server import Server
from sqlalchemy import text

class EquipamentPortsDao(object):

    # Retorna uma lista de velocidades
    @staticmethod
    def getListVelocity():
        list = []
        for velocity in db.engine.execute(text("SELECT DISTINCT velocity FROM equipament_ports")):
            if velocity[0] is None:
                continue
            list.append(velocity[0])
        return list


    # Retorna uma lista de vlans
    @staticmethod
    def getListVlans():
        list = []
        for vlan in db.engine.execute(text("SELECT DISTINCT service FROM equipament_ports")):
            if vlan[0] is None:
                continue
            list.append(vlan[0])
        return list



    # Atualiza os dados das portas
    @staticmethod
    def updateByAPI(server, port_json):
        for company in server.companies:
            for building in company.buildings:
                for floor in building.floors:
                    for room in floor.rooms:
                        for rack in room.racks:
                            for equipament in rack.equipaments:
                                # Remover porta
                                if port_json['action'] == 4:
                                    for equipament_port in equipament.equipament_ports:
                                        if equipament_port.id_port_by_api == port_json['id']:
                                            db.session.delete(equipament_port)
                                            db.session.flush()
                                            return
                                    continue
                                if equipament.id_equipament_by_api != port_json['id_parent']:
                                    continue
                                # Criar uma nova porta
                                if port_json['action'] == 2:

                                    port_old = EquipamentPorts.query.filter_by(id_port_by_api=port_json['id']).first()
                                    if port_old or port_old is not None:
                                        return

                                    equipament_port = EquipamentPorts()
                                    equipament_port.position = port_json['position']
                                    equipament_port.name = port_json['name']
                                    equipament_port.description = port_json['description']
                                    equipament_port.status = port_json['status']
                                    equipament_port.service = port_json['service']
                                    equipament_port.velocity = port_json['velocity']
                                    equipament_port.type = port_json['type_port']
                                    equipament_port.id_slot_by_api = port_json['id_slot_by_api']
                                    equipament_port.slot_name = port_json['slot_name']
                                    equipament_port.slot_description = port_json['slot_description']
                                    equipament_port.id_port_by_api = port_json['id']
                                    equipament.equipament_ports.append(equipament_port)
                                    db.session.flush()

                                    return
                                # Atualizar ou remover
                                for equipament_port in equipament.equipament_ports:
                                    if equipament_port.id_port_by_api != port_json['id']:
                                        continue
                                    # atualizar cadastro
                                    if port_json['action'] == 3:
                                        equipament_port.position = port_json['position']
                                        equipament_port.name = port_json['name']
                                        equipament_port.description = port_json['description']
                                        equipament_port.status = port_json['status']
                                        equipament_port.service = port_json['service']
                                        equipament_port.velocity = port_json['velocity']
                                        equipament_port.type = port_json['type_port']
                                        equipament_port.id_slot_by_api = port_json['id_slot_by_api']
                                        equipament_port.slot_name = port_json['slot_name']
                                        equipament_port.slot_description = port_json['slot_description']
                                        db.session.flush()
                                        return
