# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from datetime import datetime, timedelta

from model.vo.equipament import Equipament
from model.vo.server import Server


class EquipamentDao(object):


    # Atualiza os dados dos equipamentos
    @staticmethod
    def updateByAPI(server, equipament_json):
        for company in server.companies:
            for building in company.buildings:
                for floor in building.floors:
                    for room in floor.rooms:
                        for rack in room.racks:
                            if rack.id_rack_by_api != equipament_json['id_parent']:
                                continue
                            # Criar um novo equipamento
                            if equipament_json['action'] == 2:

                                equipament_old = Equipament.query.filter_by(id_equipament_by_api=equipament_json['id']).first()
                                if equipament_old or equipament_old is not None:
                                    return

                                equipament = Equipament()
                                equipament.name = equipament_json['name']
                                equipament.description = equipament_json['description']
                                equipament.ru_size = equipament_json['ru_size']
                                equipament.ru_top = equipament_json['ru_top']
                                equipament.ru_buttom = equipament_json['ru_buttom']
                                equipament.id_equipament_by_api = equipament_json['id']
                                equipament.potency_watts_usage = equipament_json['potency_watts_usage']
                                equipament.electrical_sockets_usage = equipament_json['electrical_sockets_usage']
                                equipament.created_date = datetime.strptime(equipament_json['date_log'], '%Y-%m-%d %H:%M:%S')
                                equipament.resource_type_name = equipament_json['resource_type_name']

                                if "id_equipament_parent" in equipament_json:
                                    for obj in rack.equipaments:
                                        if obj.id_equipament_by_api == equipament_json['id_equipament_parent']:
                                            equipament.id_equipament_parent = obj.id_equipament
                                            break

                                rack.equipaments.append(equipament)
                                db.session.flush()
                                return
                            # Atualizar ou remover
                            for equipament in rack.equipaments:
                                if equipament.id_equipament_by_api != equipament_json['id']:
                                    continue
                                # atualizar cadastro
                                if equipament_json['action'] == 3:
                                    equipament.name = equipament_json['name']
                                    equipament.description = equipament_json['description']
                                    equipament.ru_size = equipament_json['ru_size']
                                    equipament.ru_top = equipament_json['ru_top']
                                    equipament.ru_buttom = equipament_json['ru_buttom']
                                    equipament.potency_watts_usage = equipament_json['potency_watts_usage']
                                    equipament.electrical_sockets_usage = equipament_json['electrical_sockets_usage']
                                    equipament.created_date = datetime.strptime(equipament_json['date_log'], '%Y-%m-%d %H:%M:%S')
                                    equipament.resource_type_name = equipament_json['resource_type_name']
                                    if equipament.getService() != "Misto":
                                        service = equipament_json['service']
                                        if equipament.getService() != service:
                                            for equipament_port in equipament.equipament_ports:
                                                if equipament_port.id_slot_by_api is None:
                                                    equipament_port.service = service
                                # Remover cadastro
                                if equipament_json['action'] == 4:
                                    db.session.delete(equipament)
                                db.session.flush()
                                return
