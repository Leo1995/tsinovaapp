from TsinovaApp import db
from model.vo.group_connection import GroupConnection

class GroupConnectionDao(object):

    @staticmethod
    def insertGroupConnection(group_connection):
        db.session.add(group_connection)

    @staticmethod
    def removeGroupConnection(id_group_connection):
        group_connection = GroupConnection.query.filter_by(id_group_connection=id_group_connection).first()
        for group_rack_connection in group_connection.group_rack_connections:
            db.session.delete(group_rack_connection)
            db.session.flush()
        db.session.delete(group_connection)
        db.session.flush()
