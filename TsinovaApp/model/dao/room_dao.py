# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.room import Room
from model.vo.server import Server

from model.vo.user import User
from model.vo.server import Server
from model.vo.server_user import ServerUser
from model.vo.building import Building
from model.vo.floor import Floor
from model.vo.room import Room
from model.vo.company import Company

class RoomDao(object):


    # Retorna todos os racks de um determinado usuário
    @staticmethod
    def getRoomsByUser(id_user):
        list = []
        result = db.session.query(User, ServerUser, Server, Company, Building, Floor, Room).\
        filter(User.id_user == ServerUser.id_user).\
        filter(ServerUser.id_server == Server.id_server).\
        filter(Server.id_server == Company.id_server).\
        filter(Company.id_company == Building.id_company).\
        filter(Building.id_building == Floor.id_building).\
        filter(Floor.id_floor == Room.id_floor).\
        filter(User.id_user == id_user).\
        all()
        for obj in result:
            list.append(obj[6])
        return list


    # Atualiza os dados das salas
    @staticmethod
    def updateByAPI(server, room_json):
        for company in server.companies:
            for building in company.buildings:
                for floor in building.floors:
                    if floor.id_floor_by_api != room_json['id_parent']:
                        continue
                    # Criar uma nova sala
                    if room_json['action'] == 2:
                        room = Room()
                        room.name = room_json['name']
                        room.description = room_json['description']
                        room.id_room_by_api = room_json['id']
                        floor.rooms.append(room)
                        db.session.flush()
                        return
                    # Atualizar ou remover
                    for room in floor.rooms:
                        if room.id_room_by_api != room_json['id']:
                            continue
                        # atualizar cadastro
                        if room_json['action'] == 3:
                            room.name = room_json['name']
                            room.description = room_json['description']
                        # Remover cadastro
                        if room_json['action'] == 4:
                            db.session.delete(room)
                        db.session.flush()
                        return
