# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.building import Building
from model.vo.server import Server
from model.vo.user import User
from model.vo.server_user import ServerUser
from model.vo.building import Building
from model.vo.company import Company

class BuildingDao(object):

    # Retorna todos os prédios de um determinado usuário
    @staticmethod
    def getBuildingsByUser(id_user):
        list = []
        result = db.session.query(User, ServerUser, Server, Company, Building).\
        filter(User.id_user == ServerUser.id_user).\
        filter(ServerUser.id_server == Server.id_server).\
        filter(Server.id_server == Company.id_server).\
        filter(Company.id_company == Building.id_company).\
        filter(User.id_user == id_user).\
        all()
        for obj in result:
            list.append(obj[4])
        return list


    # Atualiza os dados dos prédios
    @staticmethod
    def updateByAPI(server, building_json):
        for company in server.companies:
            if company.id_company_by_api != building_json['id_parent']:
                continue
            # Criar um novo prédio
            if building_json['action'] == 2:
                building = Building()
                building.name = building_json['name']
                building.description = building_json['description']
                building.id_building_by_api = building_json['id']
                company.buildings.append(building)
                db.session.flush()
                return
            # Atualizar ou remover
            for building in company.buildings:
                if building.id_building_by_api != building_json['id']:
                    continue
                # atualizar cadastro
                if building_json['action'] == 3:
                    building.name = building_json['name']
                    building.description = building_json['description']
                # Remover cadastro
                if building_json['action'] == 4:
                    db.session.delete(building)
                db.session.flush()
                return
