# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from datetime import datetime, timedelta

from model.vo.rack import Rack
from model.vo.server import Server
from model.vo.user import User
from model.vo.server import Server
from model.vo.server_user import ServerUser
from model.vo.building import Building
from model.vo.floor import Floor
from model.vo.room import Room
from model.vo.rack import Rack
from model.vo.company import Company

class RackDao(object):

    # Retorna todos os racks de um determinado usuário
    @staticmethod
    def getRacksNoParticipateCalculationByUser(id_user):
        list = []
        result = db.session.query(User, ServerUser, Server, Company, Building, Floor, Room, Rack).\
        filter(User.id_user == ServerUser.id_user).\
        filter(ServerUser.id_server == Server.id_server).\
        filter(Server.id_server == Company.id_server).\
        filter(Company.id_company == Building.id_company).\
        filter(Building.id_building == Floor.id_building).\
        filter(Floor.id_floor == Room.id_floor).\
        filter(Room.id_room == Rack.id_room).\
        filter(Rack.participate_calculation == 0).\
        filter(User.id_user == id_user).\
        all()
        for obj in result:
            list.append(obj[7])
        return list

    # Retorna todos os racks de um determinado usuário
    @staticmethod
    def getRacksByUser(id_user):
        list = []
        result = db.session.query(User, ServerUser, Server, Company, Building, Floor, Room, Rack).\
        filter(User.id_user == ServerUser.id_user).\
        filter(ServerUser.id_server == Server.id_server).\
        filter(Server.id_server == Company.id_server).\
        filter(Company.id_company == Building.id_company).\
        filter(Building.id_building == Floor.id_building).\
        filter(Floor.id_floor == Room.id_floor).\
        filter(Room.id_room == Rack.id_room).\
        filter(User.id_user == id_user).\
        all()
        for obj in result:
            list.append(obj[7])
        return list


    # Atualiza os dados dos racks
    @staticmethod
    def updateByAPI(server, rack_json):
        for company in server.companies:
            for building in company.buildings:
                for floor in building.floors:
                    for room in floor.rooms:
                        # Excluir rack
                        if rack_json['action'] == 4:
                            for rack in room.racks:
                                if rack.id_rack_by_api == rack_json['id']:
                                    db.session.delete(rack)
                                    db.session.flush()
                                    return
                        else:
                            rg = None
                            if rack_json['parent_type_name'] == "Rack Group":
                                for rackgroup in room.group_racks:
                                    if rackgroup.id_group_rack_by_api == rack_json['id_parent']:
                                        rg = rackgroup
                                        break
                                if rg is None:
                                    continue
                            if rg is None and rack_json['id_parent'] != room.id_room_by_api:
                                continue
                            # Criar um novo grupo de rack
                            if rack_json['action'] == 2:
                                rack = Rack()
                                rack.id_room = room.id_room
                                rack.name = rack_json['name']
                                rack.description = rack_json['description']
                                rack.zone = rack_json['zone']
                                rack.position_zone = rack_json['position_zone']
                                rack.total_ru = rack_json['total_ru']
                                rack.created_date = datetime.strptime(rack_json['created_date']['date'], '%Y-%m-%d %H:%M:%S.%f')
                                rack.potency_watts_limit = rack_json['potency_watts_limit']
                                rack.electrical_sockets_limit = rack_json['electrical_sockets_limit']
                                rack.id_rack_by_api = rack_json['id']
                                rack.participate_calculation_fiber_ports = True
                                if rg is not None:
                                    rack.group_rack = rg
                                    rack.id_group_rack = rg.id_group_rack
                                room.racks.append(rack)
                                db.session.flush()
                                return

                            for rack in room.racks:
                                if rack.id_rack_by_api != rack_json['id']:
                                    continue
                                # atualizar cadastro
                                if rack_json['action'] == 3:
                                    rack.name = rack_json['name']
                                    rack.description = rack_json['description']
                                    rack.zone = rack_json['zone']
                                    rack.position_zone = rack_json['position_zone']
                                    rack.total_ru = rack_json['total_ru']
                                    rack.created_date = datetime.strptime(rack_json['created_date']['date'], '%Y-%m-%d %H:%M:%S.%f')
                                    rack.potency_watts_limit = rack_json['potency_watts_limit']
                                    rack.electrical_sockets_limit = rack_json['electrical_sockets_limit']
                                    db.session.flush()
                                    return
