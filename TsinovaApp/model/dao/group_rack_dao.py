# -*- Coding: UTF-8 -*-
#coding: utf-8

from TsinovaApp import db
from model.vo.group_rack import GroupRack
from model.vo.server import Server

class GroupRackDao(object):


    # Atualiza os dados dos grupos de racks
    @staticmethod
    def updateByAPI(server, rackgroup_json):
        for company in server.companies:
            for building in company.buildings:
                for floor in building.floors:
                    for room in floor.rooms:
                        if room.id_room_by_api != rackgroup_json['id_parent']:
                            continue
                        # Criar um novo grupo de rack
                        if rackgroup_json['action'] == 2:
                            rackgroup = GroupRack()
                            rackgroup.name = rackgroup_json['name']
                            rackgroup.description = rackgroup_json['description']
                            rackgroup.id_group_rack_by_api = rackgroup_json['id']
                            room.group_racks.append(rackgroup)
                            db.session.flush()
                            return
                        # Atualizar ou remover
                        for rackgroup in room.group_racks:
                            if rackgroup.id_group_rack_by_api != rackgroup_json['id']:
                                continue
                            # atualizar cadastro
                            if rackgroup_json['action'] == 3:
                                rackgroup.name = rackgroup_json['name']
                                rackgroup.description = rackgroup_json['description']
                            # Remover cadastro
                            if rackgroup_json['action'] == 4:
                                db.session.delete(rackgroup)
                            db.session.flush()
                            return
